<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("surveys", function(Blueprint $table){
                
                $table->increments("id");
                $table->integer("project_id")->unsigned();
                $table->string("link");
                $table->timestamps();
                
                $table->foreign("project_id")->references("id")->on("projects"); 
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
