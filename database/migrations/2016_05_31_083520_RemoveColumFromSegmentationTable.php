<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumFromSegmentationTable extends Migration
{
    public $project_database;
    public $segmentation;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    }

    public function removeColumn($segmentationColumnName){
        $this->segmentation = $segmentationColumnName;
        $connection = Schema::connection($this->project_database);
        $connection->table('respondents',function(Blueprint $table){
            $table->dropColumn($this->segmentation);
        });

    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
