<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOneTimeSurveyDatabase extends Migration
{
    public $project_database;
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        $connection = Schema::connection($this->project_database);
        $connection->create('question_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string("type");
            $table->boolean("choices_available");
            $table->timestamps();
        });
        $connection->create("respondents", function(Blueprint $table){
            $table->increments("id");
            $table->string("first_name");
            
            $table->string("last_name");
            $table->string("email");
            $table->timestamps();
        });

        $connection->create("categories", function(Blueprint $table){
            $table->increments("id");
            $table->string("category");
            $table->timestamps();
        });
        $connection->create("questions", function(Blueprint $table){
            $table->increments("id");
            $table->string("question");
            $table->string("short_form");
            $table->integer("category_id")->unsigned();
            $table->integer("question_type_id")->unsigned();
            $table->boolean("parent");
            $table->timestamps();
            
            $table->foreign("question_type_id")->references("id")->on("question_types");
            $table->foreign("category_id")->references("id")->on("categories");
        });
        

        // $connection->create("question_group_relations", function(Blueprint $table){
        //     $table->increments("id");
        //     $table->integer("group_id")->unsigned();
        //     $table->integer("question_id")->unsigned();
        //     $table->timestamps();
            
        //     $table->unique("question_id");
        //     $table->foreign("group_id")->references("id")->on("question_groups");
        //     $table->foreign("question_id")->references("id")->on("questions");
        // });
        $connection->create("choices", function(Blueprint $table){
            $table->increments("id");
            $table->integer("question_id")->unsigned();
            $table->integer("choice_number");
            $table->string("choice");
            
            $table->timestamps();
            
            $table->foreign("question_id")->references("id")->on("questions");
        });
        // $connection->create("conditions", function(Blueprint $table){
        //     $table->increments("id");
        //     $table->integer("question_id")->unsigned();
        //     $table->string("condition");
        //     $table->timestamps();
            
        //     $table->foreign("question_id")->references("id")->on("questions");
        // });
        $connection->create("responses", function(Blueprint $table){
            $table->increments("id");
            $table->integer("respondent_id")->unsigned();
            $table->integer("question_id")->unsigned();
            $table->string("response");
            $table->timestamps();
            
            $table->foreign("respondent_id")->references("id")->on("respondents");
            $table->foreign("question_id")->references("id")->on("questions");
        });
        $connection->create("segmentations", function(Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->string("column_name");
            $table->string("validation_rule");
            $table->timestamps();
        });

        $connection->create("segmentation_values", function(Blueprint $table){
            $table->increments("id");
            $table->integer("segmentation_id")->unsigned();
            $table->string("value");
            
            $table->timestamps();
            $table->foreign("segmentation_id")->references("id")->on("segmentations");
        });

        
        // $connection->create("respondent_segmentation_values",function(Blueprint $table){
        //     $table->increments("id");
        //     $table->integer("segmentation_id")->unsigned();
        //     $table->integer("respondent_id")->unsigned();
        //     $table->string("value");
        //     $table->timestamps();

        //     $table->foreign("respondent_id")->references("id")->on("respondents");
        //     $table->foreign("segmentation_id")->references("id")->on("segmentations");
        // });
        $connection->create("interviewers", function(Blueprint $table){
            $table->increments("id");
            $table->string("first_name");
            $table->string('last_name');
            $table->string('email');
            $table->timestamps();
        });

        $question_types = array(
            array(
                "type" => "Open Question",
                "choices_available" => false,
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now()
            ),
            array(
                "type" => "Number",
                "choices_available" => false,
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now()
            ),
            array(
                "type" => "Money Related Questions",
                "choices_available" => false,
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now()
            ),
            array(
                "type" => "Single Choice",
                "choices_available" => true,
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now()
            ),
            array(
                "type" => "Multiple Choice",
                "choices_available" => true,
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now()
            )
        );
        $databaseConnection = DB::connection($this->project_database);
        foreach ($question_types as $question_type){
            $databaseConnection->table('question_types')->insert($question_type);
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
