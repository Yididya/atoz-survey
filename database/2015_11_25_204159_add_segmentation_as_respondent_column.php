<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class AddSegmentationAsRespondentColumn extends Migration
{
    public $project_database;
    public $segmentation;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    }

    public function addSegmentation($segmentationColumnName,$segmentationName){
        $this->segmentation = $segmentationColumnName;
        $connection = Schema::connection($this->project_database);
        $connection->table('respondents',function(Blueprint $table){
            $table->string($this->segmentation);
        });
        $data = array(
            "name"=>$segmentationName,
            "column_name"=>$segmentationColumnName,
            "validation_rule"=>"",
            "created_at" => \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
            );

        $databaseConnection = DB::connection($this->project_database);
        $segmentation_id = $databaseConnection->table("segmentations")->insertGetId($data);
        return $segmentation_id;

    }
    public function getColumnNames(){
        return Schema::getColumnListing("respondents");

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
