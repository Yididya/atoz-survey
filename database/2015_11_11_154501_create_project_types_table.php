<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_conduction_interval_id')->unsigned();
            $table->string('project_type_display');
            $table->string('description');
            $table->timestamps();
            
            $table->foreign("project_conduction_interval_id")->references("id")->on("project_conduction_intervals");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_types');
    }
}
