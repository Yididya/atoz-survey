<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->string('database');
            $table->string('description');
            $table->boolean('published');
            $table->integer('project_type_id')->unsigned();
            $table->integer('conduction_type_id')->unsigned();
            $table->timestamps();
            
            $table->unique(["user_id", "title"]);
            $table->foreign("user_id")->references("id")->on("users");
            $table->foreign("conduction_type_id")->references("id")->on("conduction_types");
            $table->foreign("project_type_id")->references("id")->on("project_types");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
