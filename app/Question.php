<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ["question","short_form","question_type_id","parent","category"];
    protected $table = "questions";

    
}
