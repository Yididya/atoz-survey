<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['title','description','database','user_id','conduction_type_id','project_type_id','published'];
    protected $table ='projects';



    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function type(){
    	return $this->hasOne('App\ProjectType');
    }

    public function conduction_type(){
    	return $this->hasOne('App\ConductionType');
    }
    
}
