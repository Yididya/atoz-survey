<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//Router Pattern
$router->pattern('id', '[0-9]+');
$router->pattern('respondent_id', '[0-9]+');
$router->pattern('interviewer_id', '[0-9]+');
$router->pattern('survey_filter', '(all|published|not_published)');
$router->pattern('category_id', '[0-9]+');


//use Illuminate\Http\Request;

Route::get('/', function () {
    return view('home');
});
Route::get('home',function(){
	return view('home');
});

//Project Routes
Route::group(['middleware' =>'admin_auth_base'], function()
{
    //Newly added routes AJAX
    Route::get('/project/detail/{id}','ProjectController@detail');
    Route::get('/project/delete/{id}','ProjectController@destroy');


    Route::get('/projects/add','ProjectController@add');
    Route::get('/projects/{id}','ProjectController@show');
    Route::get('projects','ProjectController@index');

    Route::post('/projects/add','ProjectController@store');
});

//Project ConductionInterval Routes


// Authentication routes...
Route::get('/login', 'Auth\AuthController@getLogin');
Route::post('/login', 'Auth\AuthController@postLogin');
Route::get('/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('/register', 'Auth\AuthController@getRegister');
Route::post('/register', 'Auth\AuthController@postRegister');

Route::get('/auth/login', 'Auth\AuthController@getLogin');
Route::post('/auth/login', 'Auth\AuthController@postLogin');
Route::get('/auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('/auth/register', 'Auth\AuthController@getRegister');
Route::post('/auth/register', 'Auth\AuthController@postRegister');

//All the admin pages routes 
Route::group(['prefix' => 'projects/{id}','middleware' => 'admin_auth'], function()
{
	//Respondents' get routes
    Route::get('respondents','RespondentController@index');
    Route::get('respondents/add', 'RespondentController@create');
    Route::get('respondents/{respondent_id}','RespondentController@show');
    Route::get('respondents/{respondent_id}/edit','RespondentController@edit');
    Route::post('respondents/add','RespondentController@store');
	
	//Interviewers' routes 
	Route::get('interviewers','InterviewerController@index');    
	Route::get('interviewers/add','InterviewerController@create');
	Route::get('interviewers/{interviewer_id}','InterviewerController@show');
    
    Route::post('interviewers/add','InterviewerController@store');

    // Segmentation Routes
    Route::get('segmentations','SegmentationController@index');
    Route::get('segmentations/add','SegmentationController@create');
    Route::get('segmentations/{segmentation_id}','SegmentationController@show');
    Route::get('segmentations/{segmentation_id}/edit','SegmentationController@edit');
    Route::post('segmentations/add','SegmentationController@store');
    Route::get('segmentations/delete/{segmentation_id}','SegmentationController@destroy');


    //Categories Routes
    Route::get('categories','CategoryController@index');
    Route::get('categories/add','CategoryController@create');
    Route::get('categories/{category_id}','CategoryController@show');

    Route::post('categories/add','CategoryController@store');

    Route::get('questions','QuestionController@index');
    Route::get('questions/add','QuestionController@create');
    Route::get('questions/{question_id}','QuestionController@show');

    Route::post('questions/add','QuestionController@store');
    
    //Route::post('questions/load_multiple_questions','QuestionController@load_multiple_questions');    
    Route::post('questions/load_multiple_questions', "QuestionController@hello");






    

});

//Survey Routes
Route::get('surveys/success',function(){
    return view("surveys.success");

});




Route::get('surveys/{survey_filter?}','SurveyController@index');
Route::post('surveys/{survey_filter?}','SurveyController@search');

Route::get('surveys/{survey_link}','SurveyController@survey');

Route::get('surveys/{id}/publish','SurveyController@publish');


Route::group(['middleware' =>'admin_auth'], function()
{
    Route::get('surveys/{id}/publish','SurveyController@publish');   
    Route::post('surveys/{id}/publish','SurveyController@update_project');
    Route::get('surveys/{id}/unpublish','SurveyController@unpublish');
    Route::post('surveys/{id}/unpublish','SurveyController@unpublish_project');
});

//Survey form submitted
Route::post('surveys/{survey_link}','SurveyController@submit');



//Analysis routes
Route::get('analysis','AnalysisController@index');    
Route::post('/analysis/list_child_questions','AnalysisController@list_child_questions');
Route::get('analysis/{survey_link}/{category_id?}','AnalysisController@analysis');
Route::post('analysis/{survey_link}/{category_id?}','AnalysisController@report_survey');



Route::get('surveys/register/{survey_link}','SurveyController@register');
Route::post('surveys/add/{survey_link}','SurveyController@add_respondent');




//Test Routes
Route::get('test','AnalysisController@trail');    

//Learn more route
Route::get('about',function(){
    return view('pages.about');
});



//Here are the links for the apis

Route::group(['prefix' => 'api/{API_KEY}/','middleware' => 'admin_auth'], function()
{
    Route::get('projects','ProjectController@index');

});