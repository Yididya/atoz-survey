<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Project;
use App\Category;
use App\Question;
use App\Http\Controllers\Controller;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Http\Requests\CreateCategoryRequest;

use Carbon\Carbon;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        
        $project = Project::find($id);
        $this->configure_database($project->database);   
        $categories = Category::on($project->database)->get();
        return view("categories.index" ,compact('project','categories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $project = Project::find($id);
        
        return view("categories.add",compact("project"));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request,$id)
    {
        $project = Project::find($id);
        $this->configure_database($project->database);
        $category = $request->only('category');
        $category["created_at"] = Carbon::now();
        $category["updated_at"] = Carbon::now();


        Category::on($project->database)->insert($category);
        return redirect('/projects/'.$project->id.'/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $category_id)
    {
        $project = Project::find($id);
        $this->configure_database($project->database);
        
        $category = Category::on($project->database)->find($category_id);
        $questions = Question::on($project->database)->where("category_id","=",$category->id)->get();
        
        //dd($questions);
        return view("categories.show",compact("project","category","questions"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function configure_database($database){
        Config::set("database.connections.$database", array(
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => "$database",
            'username'  => 'root',
            'password'  => '0000',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ));
    }
}
