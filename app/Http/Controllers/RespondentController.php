<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

use App\Respondent;
use App\Project;
use App\Segmentation;
use App\SegmentationValue;
use Illuminate\Support\Facades\Config;
use App\Http\Requests\CreateRespondentRequest;
class RespondentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //Fetch the project
        $project = Project::find($id);
        //Configure Database and fetch all respondents 
        $this->configure_database($project->database);
        $respondents = Respondent::on($project->database)->get();
        //dd($respondents);
        return view('respondents.index',compact('project','respondents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //Fetch the project
        $project = Project::find($id);
        
        //Configure Database and fetch all respondents 
        $this->configure_database($project->database);
        
        //Query the database for any segmentations
        $segmentations = Segmentation::on($project->database)->lists("name","id")->toArray();
        //dd($segmentations);
        //dd(Segmentation::on($project->database)->first()->values);
        //Initialize a segmentation values array
        $segmentation_values = array();
        //Populate segmentation values to each segmentation
        foreach ($segmentations as $key => $value) {
            $segmentation_value[$value] = SegmentationValue::on($project->database)->lists("value","id")->toArray();
        }
        // dd($segmentation_value);
        //dd($segmentations->values());
        return view('respondents.add',compact('project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRespondentRequest $request,$id)
    {

        $project = Project::find($id);

        //Configure Database and fetch all respondents 
        $this->configure_database($project->database);

        //Filtering requests for Respondent Table
        $respondent_fields = $request->only('first_name','last_name');
        
        //Filtering Request for Segmentation_respondents table
        $segmentation_fields = $request->except('_token','first_name','last_name');

        //Insert into Respondent Table
        Respondent::on($project->database)->insert($respondent_fields);
        
        return redirect('/projects/'.$project->id.'/respondents');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Fetch the project
        $project = Project::find($id);
        
        //Configure Database and fetch all respondents 
        $this->configure_database($project->database);
        
        //Query the database for any segmentations
        $segmentations = Segmentation::on($project->database)->get();

        //dd(Segmentation::on($project->database)->first()->values);
        //Initialize a segmentation values array
        $segmentation_values = array();
        //Populate segmentation values to each segmentation
        foreach ($segmentations as $value) {
            
        }

        //dd($segmentations->values());
        return view('respondents.edit',compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function configure_database($database){
        Config::set("database.connections.$database", array(
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => "$database",
            'username'  => 'root',
            'password'  => '0000',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ));
    }
}
