<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateQuestionRequest;

use App\Project;
use App\Question;
use App\QuestionType;
use App\Category;
use App\Choice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class QuestionController extends Controller
{
    const DEFAULT_VALUE = 0;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $project = Project::find($id);
        $this->configure_database($project->database);
        $questions = Question::on($project->database)->paginate(5);
        
        
        
        return view('questions.index',compact("project","questions"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {   
        $project = Project::find($id);
        $this->configure_database($project->database);
        
        //Fetch necessary fields for question form
        $question_types = QuestionType::on($project->database)->lists('type','id')->toArray();
        $categories = Category::on($project->database)->lists('category','id')->toArray();
        //Fetch questions 
        return view('questions.add',compact('project','question_types','categories'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateQuestionRequest $request,$id)
    {
        // $array = array(12,4,5,6);
        // array_splice ($array, 2,1);
        // dd($array);

        //dd($request->all());
        $project = Project::find($id);

        $question = $request->except('_token',"choice_values");
        $question_type_id = $question["question_type_id"];
        

        //Get the array of choices
        $choices = $request->only("choice_values");
        if(!empty($choices["choice_values"])){
            $choices = array_filter($choices["choice_values"]);    
        }
        
        //dd(array_filter($choices));
        

        


        //dd($choices);
        // //Discard choice if empty choice_values=>{"","",""}
        // if(!empty($choices)){

        //     for ($i=0; $i < count($choices) ; $i++) { 
        //         //dd(count($choices));
        //         if(empty($choices[$i])){
        //             //dd();
        //             array_splice($choices, $i ,1);
        //         }
        //     }
            
        
        // }
        
        // dd($choices);


        //Set timestamps for the question to be inserted
        $question["created_at"] = Carbon::now();
        $question["updated_at"] = Carbon::now();
        


        //Set the category_id and the parent id to DEFAULT_VALUE if nothing specified;
        if(empty($question["category_id"])){
            $question["category_id"] = self::DEFAULT_VALUE;
        }
        if(empty($question["parent"])){
            //Default value set to zero
            $question["parent"] = self::DEFAULT_VALUE;
        }





        //Configure database for insertion into database
        $this->configure_database($project->database);
        $question_types = QuestionType::on($project->database)->lists('type','id')->toArray();
        //dd($choices);


        //Check if it has no choices and it is a multiple and Single Question
        if(($question_types[$question_type_id]  === "Single Choice" || $question_types[$question_type_id]  === "Multiple Choice") && empty($question["parent"] ) ){
            if(empty($choices)){
                return redirect()->back()->withInput()->withErrors(["A Multiple/Single Choice question types must have their choices specified"]);
            }
        }
        //dd("No");


        //Get Inserted Question ID
        $question_id = Question::on($project->database)->insertGetId($question);



        //dd($question_id);

        //Choices to database
        //Get list of question types
        $question_types = QuestionType::on($project->database)->lists('type','id')->toArray();
        if(($question_types[$question_type_id]  === "Single Choice" || $question_types[$question_type_id]  === "Multiple Choice") && empty($question["parent"] )){
            
            //dd($choices);
            if(!empty($choices) and !empty($question_id)){
                for($i = 0; $i < count($choices); $i++){
                
                    $choice = array();
                    $choice["choice_number"] = $i + 1;
                    $choice["question_id"] = $question_id;
                    $choice["choice"] = $choices[$i];
                    $choice["created_at"] = Carbon::now();
                    $choice["updated_at"] = Carbon::now();
                    //print_r($choice);
                    Choice::on($project->database)->insert($choice);
                    //dd("Was here");

                }    
            }    
        }

        
        
        
        return redirect('/projects/'.$project->id.'/questions');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function configure_database($database){
        Config::set("database.connections.$database", array(
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => "$database",
            'username'  => 'root',
            'password'  => '0000',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ));
    }
    public function hello(Request $request ,$id){
        $project = Project::find($id);

        $category_id = $request->category;
        //dd($category_id);        

        $this->configure_database($project->database);   
        $questions = Question::on($project->database)->where(["category_id"=>$category_id,"question_type_id"=>5])->lists("short_form","id")->toArray();
        return json_encode($questions);
    }
}
