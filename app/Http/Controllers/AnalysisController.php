<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Project;
use App\Survey;
use App\Category;
use App\Response;
use App\Segmentation;
use App\Question;
use App\QuestionType;
use App\Choice;
use App\SegmentationValue;
use App\Respondent;
use App\RespondentSegmentationValue;
use Auth;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class AnalysisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            
                
            $projects = Project::where(['user_id'=>Auth::id(),'published'=>1])->get(); // Temporary Id 1     
                
            
            
            
            foreach ($projects as  $project) {
                $survey = Survey::where(['project_id'=> $project->id])->first();  
                if(!empty($survey)){
                    $project->survey = $survey;    
                }
            }      
            $projects = $projects->toArray();

            
            return view('analysis.index',compact("projects"));
        }
        return redirect('/login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function trail(Request $request){
        //dd(Project::where(['published'=>1])->where('title','LIKE','%addis%')->get());
        return view('projects.trail');



        
        $project = $request->session()->get('project');
        $this->configure_database($project->database);


        dd(Question::on($project->database)->orWhere(function($query){
            $query->where(['category_id'=>1])
                  ->whereNotIn('parent',[15]);
        })->orWhere(function($query){
            $query->where(['category_id'=>2])
                  ->where(['parent'=> 0]);
        })->count());

        $question_id = 14;
        $segmentation_id = 2;

        

        dd(RespondentSegmentationValue::on($project->database));


        $segmentation_values = SegmentationValue::on($project->database)->where(['segmentation_id'=>$segmentation_id])->get();
        $choices = Choice::on($project->database)->where(['question_id'=>$question_id])->get();

        //dd($choices);
        $response = array();



        foreach ($segmentation_values as $value) {
                $response[$value->value] = array();
                foreach ($choices as $choice) {
                    $response[$value->value][$choice->choice] = RespondentSegmentationValue::on($project->database)->where(['segmentation_id'=>$segmentation_id,'value'=>$value->value])->join('responses','respondent_segmentation_values.respondent_id','=','responses.respondent_id')->where(['response'=>$choice->choice_number,'question_id'=>$question_id])->get()->count();
                    
                }
                
                
            
        }
        return json_encode($response);
       
    }
    public function report_survey(Request $request){
        

        if(Auth::check()){
            //dd("was here");
            //Get options from request
            $options = $request->only('options');
            //Check if filter array is available

            //dd($options);


            $segmentation_id = $options["options"]["segmentationId"];
            $question_id = $options["options"]["questionId"];
            $question_type = $options["options"]["questionType"];

            $filter_array = array();
            
            if(isset($options["options"]["filterArray"])){
                $filter_array = $options["options"]["filterArray"];
            }

            //dd($filter_array);
            
            $parent_question_id = $options["options"]["parentQuestionId"];
            

            //Get the project from session
            $project = $request->session()->get('project');
            $this->configure_database($project->database);

            //initialize the builder here
            $builder = Respondent::on($project->database);
            
            if(!empty($filter_array)){
                
                foreach ($filter_array as $key => $value) {

                    $segmentation = Segmentation::on($project->database)->where(['id'=> $key])->first();
                    foreach ($value as $val) {
                        $builder = $builder->where($segmentation->column_name,'<>',$val);
                    }
                    //$builder = $builder->orWhereNotIn($segmentation->column_name,$value);
                    
                    //     $query->whereNotIn('value',$value);
                    
                    // $builder = $builder->orWhere(function($query) use($key,$value){
                    //     $query->whereNotIn('value',$value);
                    // });
                }
            }
            //return $builder->get()->toArray();
            //dd($builder->count());
            

            //Select Segmentation values 
            $segmentation_values = SegmentationValue::on($project->database)->where(['segmentation_id'=> $segmentation_id])->get();
            

            //Get list of Choices
            $choices = Choice::on($project->database)->where(['question_id'=>$question_id])->get();
            if($parent_question_id != 0){
                $choices = Choice::on($project->database)->where(['question_id'=>$parent_question_id])->get();
            }

            //Prepare output arrays
            $response = array();
            $label = array();

            foreach ($segmentation_values as $value) {        
                $label[] = $value->value;
            }
            foreach ($choices as $choice) {
            
                    $response[$choice->choice] = array();
                    $builder_looper  = clone $builder;
                    foreach ($segmentation_values as $value) {     
                        $segmentation = Segmentation::on($project->database)->where(['id'=> $segmentation_id])->first();

                        $response[$choice->choice][$value->value] = $builder_looper->where($segmentation->column_name ,$value->value)->join('responses','respondents.id', '=','responses.respondent_id')->where(['question_id'=>$question_id,'response'=>$choice->choice_number])->count();
                        //$response[$choice->choice][$value->value] = $builder_looper->where(['segmentation_id'=>$segmentation_id,'value'=>$value->value])->join('responses', 'respondent_segmentation_values.respondent_id','=','responses.respondent_id')->where(['question_id'=>$question_id,'response'=>$choice->choice_number])->count();
                        $builder_looper = clone $builder;
                    }
            }

            if(!empty($choices)){
                $response_json['label'] = $label;
                $response_json['legend'] = array_keys($response);
                $response_json['data'] = $response;
            }





            return json_encode($response_json);


        }
        return redirect('/login');
        

    }
    public function list_child_questions(Request $request){
        //Get the project from the session[SESSION set  AnalysisController@analysis]
        if(!empty($request->session()->get('project')) && !empty($request->questionId) ){
            $project = $request->session()->get('project');
            $this->configure_database($project->database);

            $child_questions = Question::on($project->database)->where(['parent'=>$request->questionId])->get();
            $question_types = QuestionType::on($project->database)->lists("type","id");
            foreach ($child_questions as $question) {
                $question->question_type = $question_types[$question->question_type_id];
            }
            return json_encode($child_questions);
        }

    }
    public function analysis(Request $request,$survey_link,$category_id = null){
        
        if(Auth::check()){
            $survey = Survey::where(['link'=>$survey_link])->first();
            $project = Project::where(['id'=>$survey->project_id])->first();
            
            if(!empty($project)){
                //$this->configure_database($project->database);
                //Check if project belongs to this specific user
                if($project->user_id == Auth::id()){


                        //Configure database
                        $this->configure_database($project->database);
                        //Get categories
                        $categories = Category::on($project->database)->get();

                        $segmentations = Segmentation::on($project->database)->get();


                        //Fetch segmentation values foreach segmentations
                        foreach ($segmentations as $segmentation) {
                            $values = SegmentationValue::on($project->database)->where(['segmentation_id'=>$segmentation->id])->get();
                            $segmentation->values = $values;                            
                        }


                        //Check if category_id is set 
                        if(empty($category_id)){
                            //Pick on from categories
                            $category_id = $categories->first()->id;
                        }
                        $category = $categories->find($category_id);                     

                        
                        //Lookup Questions
                        $questions = Question::on($project->database)->where(['category_id'=>$category->id , 'parent'=> 0])->get();

                        //Questions Types
                        $question_types = QuestionType::on($project->database)->lists("id","type");

                        //Get choices if any 
                        foreach ($questions as $question) {
                            
                            if($question->type == $question_types["Open Question"]  ){

                            }else if($question->question_type_id == $question_types["Single Choice"]){
                                //Check if the question have choices 
                                $choices = Choice::on($project->database)->lists("choice","choice_number");
                                $question->choices = $choices;
                            }else if($question->question_type_id == $question_types["Multiple Choice"]){
                                //Collect Child Questions

                                $children_questions = Question::on($project->database)->where(['parent'=>$question->id])->get();
                                $question->children_questions = $children_questions;

                                //Check if the question have choices 
                                $choices = Choice::on($project->database)->lists("choice","choice_number");
                                $question->choices = $choices;
                                //dd($question->children_questions);
                            }else{
                                
                            }
                        }
                        //Set questions to each category 
                        $category->questions = $questions;
                        
                        //Question Types
                        $question_types = QuestionType::on($project->database)->lists("type","id");

                        //Add the survey To session
                        $request->session()->put('project',$project);

                        return view('analysis.analysis',compact("category","categories","segmentations","question_types","survey"));
                    
                        

    
                        
                  



                }else{
                    abort(404,"This survey does not belong to you");
                }

            }else{
                abort(404);
            }
            
        }
    }

    public function configure_database($database){
        Config::set("database.connections.$database", array(
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => "$database",
            'username'  => 'root',
            'password'  => '0000',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ));
    }
}
