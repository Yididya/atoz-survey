<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Project;
use App\Respondent;
use App\Segmentation;
use App\SegmentationValue;
use Illuminate\Support\Facades\Config;
use App\Http\Requests\CreateSegmentationRequest;
use Carbon\Carbon;
use AddSegmentationAsRespondentColumn;
use RemoveColumFromSegmentationTable;


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class SegmentationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $project = Project::find($id);
        $this->configure_database($project->database);

        $segmentations = Segmentation::on($project->database)->get();

        return view('segmentations.index',compact('segmentations','project'));

        //dd($segmentations->toArray());

        //dd("Segmentation Controller index method");
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //Fetch the project
        $project = Project::find($id);
        //Configure Database and fetch all respondents 
        $this->configure_database($project->database);
        //$segmentations = Segmentation::on($project->database)->get();
        //dd($respondents);

        
        
        return view('segmentations.add',compact('project'));
    }
    public function insertSegmentationValues($database,$segmentation_values,$segmentation_id){
        if(!empty($segmentation_values["segmentation_value"])){
            foreach ($segmentation_values["segmentation_value"] as $key => $value) {
                $segmentation_value = array();
                $segmentation_value["segmentation_id"] = $segmentation_id;
                $segmentation_value["value"] = $value;
                $segmentation_value["created_at"] = Carbon::now();
                $segmentation_value["updated_at"] = Carbon::now();
                SegmentationValue::on($database)->insert($segmentation_value);
            }

        }

    }



    public function generateColumnName($segmentation,$project){

        $columnMigration = new AddSegmentationAsRespondentColumn();
        $columns = $columnMigration->getColumnNames();
        
        $additionalColumns = Segmentation::on($project->database)->lists('column_name')->toArray();
        
        $columns = array_merge($columns, $additionalColumns);
        //dd($columns);

        $name = strtolower(str_replace(" ", "_", $segmentation));
        $column_name = $name;

        $count = 1;

        while(true){
            $found = false;
            foreach ($columns as $column) {
                if($column_name == $column){
                    $found = true;
                    break;
                }
                
            }
            if (!$found){

                return $column_name;
            }
            $column_name = $name . "_$count";
            $count++;
        }
        //dd($column_name);
        return $column_name.hash("ripemd128", $segmentation);
    }
    public function addSegmentationToDatabase($project,Request $request){
        $input = $request->all();
        $segmentation_values = $input["segmentation_value"];

        $input["column_name"] = $this->generateColumnName($input["name"],$project);
        



        $addSegmentationToRespondentColumn =  new AddSegmentationAsRespondentColumn ();
        $addSegmentationToRespondentColumn->project_database = $project->database;
        $segmentation_id = $addSegmentationToRespondentColumn->addSegmentation($input["column_name"],$input["name"]);
        //dd($segmentation_id);

        if(!empty($segmentation_id) and !empty($segmentation_values)){
            foreach($segmentation_values as $value){
            if(!empty($value)){
                $row = array(
                    'segmentation_id' => $segmentation_id,
                    'value' => $value,
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now(),
                );
                SegmentationValue::on($project->database)->insert($row);    
            }
            

        }

        }
        


    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSegmentationRequest $request,$id)
    {
        $project = Project::find($id);
        //Configure Database and fetch all respondents 
        $this->configure_database($project->database);

        $input = $request->all();

        //Check if the request contains segmentation values
        if(empty($input["segmentation_value"])){
            //redirect back with INput
            return redirect()->back()->withInput()->withErrors(["The free version does not support a filter with out its values defined. Use the + button to add values to your filter."]);
        }


        //dd($this->generateColumnName($project->database, $request->name));
        //dd($request->all());

        $this->addSegmentationToDatabase($project,$request);
        
        //dd($request);
        //Fetch if any segmentation values
        // $segmentation_values = $request->except('_token','name','description');


        
        
        
        // //Filtering requests for Segmentation Table
        // $segmentation = $request->only('name');
        // //Add type to the segmentation
        // if(empty($segmentation_values["segmentation_value"])){

        //     $segmentation["type"] = "general";
        // }else{
        //     $segmentation["type"] = "predefined";
        // }
        // $segmentation["created_at"] = Carbon::now();
        // $segmentation["updated_at"] = Carbon::now();

        // //Insert into Respondent Table
        // $segmentation_id = Segmentation::on($project->database)->insertGetId($segmentation);
        // $this->insertSegmentationValues($project->database,$segmentation_values,$segmentation_id);
        // //SegmentationValues::on($project->database)->insert($segmentation_values["segmentation_value"]);
        
        // //Filtering Request for Segmentation_respondents table
        // $segmentation_fields = $request->except('_token','first_name','last_name');

        
        
        return redirect('/projects/'.$project->id.'/segmentations');
        //dd($request->except('_token','name','description'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$segmentation_id)
    {
        
        $project = Project::find($id);
        $this->configure_database($project->database);
        $segmentation = Segmentation::on($project->database)->find($segmentation_id);
        //dd($segmentation->type);
        //Fetch predefined values if any 
        
        $segmentation_values = SegmentationValue::on($project->database)->where("segmentation_id", "=" ,$segmentation_id)->get();
        
        //dd($segmentation_values);        
        
        return view('segmentations.show',compact('segmentation','segmentation_values','project'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$segmentation_id)
    {
            $project = Project::find($id);
            $this->configure_database($project->database);
            $segmentation = Segmentation::on($project->database)->where(["id"=>$segmentation_id])->first();
            //dd($segmentation->column_name);
            //Delting values in Segmentation value and segmentations
            SegmentationValue::on($project->database)->where(['segmentation_id'=>$segmentation_id])->delete();
            Segmentation::on($project->database)->where(["id"=>$segmentation_id])->delete();
            //Droping the column 
            $removingColumMigration = new RemoveColumFromSegmentationTable ();
            $removingColumMigration->project_database = $project->database;
            $removingColumMigration->removeColumn($segmentation->column_name);


        
        




        return 200;
    }
    public function configure_database($database){
        Config::set("database.connections.$database", array(
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => "$database",
            'username'  => 'root',
            'password'  => '0000',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ));
    }
}
