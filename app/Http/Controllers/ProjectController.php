<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
//Use the Project Model
use App\Project;
use App\Category;
use App\Question;
use App\Segmentation;
use App\ProjectType;
use App\ConductionType;
use App\Http\Requests\CreateProjectRequest;
use Carbon\Carbon;
use CreateOneTimeSurveyDatabase;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request,$id){
        $project = Project::where(['user_id'=>Auth::id(),'id'=>$id])->first();
        
        $this->configure_database($project->database);
        $categories = Category::on($project->database)->count();
        $questions = Question::on($project->database)->count();

        $project["categories_count"] = $categories;
        $project["questions_count"] = $questions;



        
        return $project->toJson();

    }
    public function index()
    {
        if(Auth::check()){

            //Authentication to user user_id = Auth::user_id;
            $projects = Project::where(['user_id'=>Auth::id()])->whereNull('deactivation_date')->paginate(5); // Temporary Id 1 


            return view('projects.index', compact('projects'));
        }
        return redirect('/');
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProjectRequest $request)
    {
        //dd($request->all());
        if(Auth::check()){
            $project = $request->all();

            $project["database"] = $this->generate_database_name($request->title);

            //Check if database name is okay
            //dd(preg_match("[0-9a-zA-Z$_]+", "abcd" ));
            
            if(!preg_match("/[0-9a-zA-Z\_]+/" , $project["database"])){
                return redirect('/projects/add')->withInput()->with("database_name_error","The project title should only contain letters and numbers");
            }
            //dd($project["database"]);

            $project["user_id"] = Auth::id();
            //Set published to false 
            $project["published"] = 0;
            

            //dd($request);
            //$input["database"] = $request->title; // Temporary Database Name
            //$input["user_id"] = Auth::id();
            //dd($input);
            Project::create($project);
            //Create Database and tables 

            $this->create_database($project["database"], $project["project_type_id"]);

            return redirect('/projects');
        }
        
        
        //return $this->createDatabase($input["database"], $input["project_type_id"]);
    }

    public function add(){
        $conduction_type_collection = ConductionType::all(['id','type']);
        $project_type_collection = ProjectType::all(['type','id']);
        $project_type = array();
        $conduction_type = array();

        foreach ($project_type_collection as $value) {
            
            $project_type[$value->toArray()["id"]] = $value->toArray()["type"];
        }
        foreach ($conduction_type_collection as $value) {
            
            $conduction_type[$value->toArray()["id"]] = $value->toArray()["type"];
        }
        //dd($project_type);
        //dd($conduction_type);
        return view('projects.add',compact('conduction_type','project_type'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::findOrFail($id);
        //dd($project);
        $this->configure_database($project->database);

        $project->categories_count = Category::on($project->database)->count();
        $project->questions_count = Question::on($project->database)->count();
        $project->segmentations_count = Segmentation::on($project->database)->count();
    
        return view('projects.show',compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::where(['user_id'=>Auth::id(),'id'=>$id])->update(['deactivation_date'=>Carbon::now()]);
        return 200;
        
    }

    public function createProject(Request $request){
        $input = $request->all();
        $input["user_id"] = 1;
        $input["title"] = trim($input['title']);
        $input["description"] = trim($input['description']);
        $input["database"] = $this->generateDatabaseName($input["title"]);
        $input["created_at"] = Carbon::now();
        $input["updated_at"] = Carbon::now();
        Project::create($input);
        return $this->createDatabase($input["database"], $input["project_type_id"]);
    }

    public function generate_database_name($project_title){
        $project_database = strtolower(str_replace(" ", "_", $project_title));
        $res = Project::where("database", $project_database)->get();
        $count = 1;
        while(count($res) >=1){
            $project_database = strtolower(str_replace(" ", "_", $project_title)) . "_$count";
            $res = Project::where("database", $project_database)->get();
            $count++;
        }
        return $project_database;
    }
    /*
     * configure the database to be used
     *
     * @param string database
     */
    public function configure_database($database){
        Config::set("database.connections.$database", array(
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => "$database",
            'username'  => 'root',
            'password'  => '0000',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ));
    }
    /*
     * Create database for the project using the appropriate properties from the project type.
     *
     * @param string $project_database (generated database name), int $project_type_id
     */
    public function create_database($project_database, $project_type_id)
    {
        //If elses for Project type_id;
        $project_type = ProjectType::where(['id'=>$project_type_id])->first();
        //dd($project_type->type);
        DB::statement("CREATE DATABASE $project_database");
        $this->configure_database($project_database);

        if($project_type->type == "One time survey"){
            $createOneTimeSurveyDatabase = new CreateOneTimeSurveyDatabase();
            $createOneTimeSurveyDatabase->project_database = $project_database;
            //dd($createOneTimeSurveyDatabase);
            $createOneTimeSurveyDatabase->up();
            
        }
    }
}
