<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Project;
use App\Category;
use App\Question;
use App\Survey;
use App\QuestionType;
use App\Choice;
use App\Response;
use App\Segmentation;
use App\SegmentationValue;
use App\RespondentSegmentationValue;
use App\Respondent;


use App\Http\Requests\CreateRespondentRequest;

use Carbon\Carbon;


use Auth;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($survey_filter = null)
    {


        if(Auth::check()){
            if(!empty($survey_filter)){
                if($survey_filter == "published"){
                    $projects = Project::where(['user_id'=>Auth::id(),'published'=>1])->paginate(5); // Temporary Id 1     
                }else if($survey_filter == "not_published"){
                    $projects = Project::where(['user_id'=>Auth::id(),'published'=>0])->paginate(5); // Temporary Id 1     
                }
            }else{
                $projects = Project::where(['user_id'=>Auth::id()])->paginate(5); // Temporary Id 1 
            }
            
            foreach ($projects as  $project) {
                $survey = Survey::where(['project_id'=> $project->id])->first();  
                if(!empty($survey)){
                    $project->survey = $survey;    
                }
            }      
            //$projects = $projects->toArray();

            
            return view('surveys.index',compact("projects"));
        }else{
            $projects = Project::where(['published'=>1])->paginate(5); // Temporary Id 1     
            //dd($projects);
            foreach ($projects as  $project) {
                $survey = Survey::where(['project_id'=> $project->id])->first();  
                if(!empty($survey)){
                    $project->survey = $survey;    
                }
            }      
            //$projects = $projects->toArray();
            

            return view('surveys.guest_index',compact("projects"));    
        }
        

    }
    public function search(Request $request,$survey_filter = null){
        //Get the query string
        $query = $request->only('q');

        if(empty($query['q'])){
            dd("empty(var)");
           return redirect("/surveys");
        }
        
        if(Auth::check()){
            if(!empty($survey_filter)){
                if($survey_filter == "published"){
                    $projects = Project::where(['user_id'=>Auth::id(),'published'=>1])->where('title','LIKE',"%".$query["q"]."%")->paginate(5); // Temporary Id 1     
                }else if($survey_filter == "not_published"){
                    $projects = Project::where(['user_id'=>Auth::id(),'published'=>0])->where('title','LIKE',"%".$query["q"]."%")->paginate(5); // Temporary Id 1     
                }
            }else{
                $projects = Project::where(['user_id'=>Auth::id()])->where('title','LIKE',"%".$query["q"]."%")->paginate(5); // Temporary Id 1 
            }
            
            foreach ($projects as  $project) {
                $survey = Survey::where(['project_id'=> $project->id])->first();  
                if(!empty($survey)){
                    $project->survey = $survey;    
                }
            }      
            //$projects = $projects->toArray();

            
            return view('surveys.index',compact("projects","query"));
        }else{
            $projects = Project::where(['published'=>1])->where('title','LIKE',"%".($query["q"])."%")->paginate(5); // Temporary Id 1     
            //dd($projects);
            foreach ($projects as  $project) {
                $survey = Survey::where(['project_id'=> $project->id])->first();  
                if(!empty($survey)){
                    $project->survey = $survey;    
                }
            }      
            //$projects = $projects->toArray();


            return view('surveys.guest_index',compact("projects","query"));    
        }

    }
    public function publish($id){
        $project  = Project::find($id);
        $this->configure_database($project->database);
        $category_count = Category::on($project->database)->count();
        $question_count = Question::on($project->database)->count();

        

        return view("surveys.publish",compact("id","project","category_count","question_count"));

    }

    public function unpublish($id){
        $project  = Project::find($id);
        $this->configure_database($project->database);
        $category_count = Category::on($project->database)->count();
        $question_count = Question::on($project->database)->count();

        Survey::where(['project_id'=>$id])->delete();

        return view("surveys.unpublish",compact("id","project","category_count","question_count"));

    }
    public function update_project($id){
        $project = Project::find($id);
        $project->published = 1;
        $project->save();

        //Create a new row in surveys table
        $survey = array('project_id'=>$id,'link'=>$project->database,'created_at'=>Carbon::now(),'updated_at'=> Carbon::now());
        Survey::insertGetId($survey);

        return 200;
    }
    public function unpublish_project($id){
        $project = Project::find($id);
        $project->published = 0;
        $project->save();

        Survey::where(["project_id" => $project->id])->delete();


        return 200;
    }
    public function survey(Request $request, $survey_link){

        //dd($request->session()->get('interviewee.survey'));
        $survey = Survey::where(['link'=>$survey_link])->first();
        //$request->session()->push('interviewee.surveys', $survey->id);
        //dd($survey);

        if(!empty($survey)){

            //Check if the interviewee has already filled out basic information for this specific survey
            if(!empty($request->session()->get('interviewee.surveys'))){
                //User has filled out couple of times
                

                $surveys_to_go = $request->session()->get('interviewee.surveys');

                if(array_search($survey->id, array_keys($surveys_to_go)) === false){
                    //User has already submited the form
                    return redirect('/surveys/register/'.$survey_link);   
                }



            }else{
                
                return redirect('/surveys/register/'.$survey_link);
            }
        }else{
            abort(404,"Survey not found");
        }
        //dd($survey_link);
        //dd($request);
        // $project_id  = Survey::where(['link'=>$survey_link])->first()->project_id;
        
        // if(!empty($project_id)){
        //     $project = Project::where(['id'=> $project_id])->first();
        //     //Configure database
        //     $this->configure_database($project->database);
        //     //Get categories
        //     $categories = Category::on($project->database)->get();
        //     $categories_array = $categories->toArray();

        //     //Pick a category 
        //     $current = (isset($requet->current) == false ? 0: $request->current);
            
        //     if(count($categories_array) != 0) {

        //         if(!empty($categories_array[$current])){
        //             $category = Category::on($project->database)->where(["id" => $categories_array[$current]["id"]])->first();
                    
        //             //Do your category thing here
        //             $questions = Question::on($project->database)->where(['category_id'=>$category->id , 'parent'=> 0])->get();
        //             //Get choices if any 
        //             foreach ($questions as $question) {
        //                 //Questions Types
        //                 $question_types = QuestionType::on($project->database)->lists("id","type");
        //                 if($question->type == $question_types["Open Question"]  ){

        //                 }else if($question->question_type_id == $question_types["Single Choice"]){
        //                     //Check if the question have choices 
        //                     $choices = Choice::on($project->database)->where(['question_id'=>$question->id])->lists("choice","choice_number");
        //                     $question->choices = $choices;
        //                 }else if($question->question_type_id == $question_types["Multiple Choice"]){
        //                     //Collect Child Questions

        //                     $children_questions = Question::on($project->database)->where(['parent'=>$question->id])->get();
        //                     $question->children_questions = $children_questions;

        //                     //Check if the question have choices 
        //                     $choices = Choice::on($project->database)->where(['question_id'=>$question->id])->lists("choice","choice_number");
        //                     $question->choices = $choices;
        //                     //dd($question->children_questions);
        //                 }else{
                            
        //                 }

        //             }
        //             //Set questions to each category 
        //             $category->questions = $questions;    
                    

                



        //         }else{
        //             abort(404,"Survey section not found");
        //         }
        //     }else{
        //         abort(404,"Survey not found");
        //     }


        //     // //Get questions for each category
        //     // foreach ($categories as $category) {
        //     //     //Lookup Questions
                
                

        //     // }
            
        //     $questions = Question::on($project->database)->get();
        //     //dd($category->questions);
        //     return view('surveys.survey',compact('category','project','current'));

        //dd($survey_link);
        // }else{
            
        // }

        //Check if the user has already filled out these form 
        if(!is_null($request->session()->get('interviewee.completed_surveys'))){
            if(array_search($survey->id,$request->session()->get('interviewee.completed_surveys')) === False){
                return redirect('/surveys')->with('message',"You have already filled out the survey");
            }    
        }
        

        //Check if user has already been registered 
        $project_id  = Survey::where(['link'=>$survey_link])->first()->project_id;
        
        if(!empty($project_id)){
            $project = Project::where(['id'=> $project_id])->first();
            //Configure database
            $this->configure_database($project->database);
            //Get categories
            $categories = Category::on($project->database)->get();
            


            //Get questions for each category
            foreach ($categories as $category) {
                //Lookup Questions
                $questions = Question::on($project->database)->where(['category_id'=>$category->id , 'parent'=> 0])->get();
                //Get choices if any 
                foreach ($questions as $question) {
                    //Questions Types
                    $question_types = QuestionType::on($project->database)->lists("id","type");
                    if($question->type == $question_types["Open Question"]  ){

                    }else if($question->question_type_id == $question_types["Single Choice"]){
                        //Check if the question have choices 
                        $choices = Choice::on($project->database)->where(['question_id'=>$question->id])->lists("choice","choice_number");
                        $question->choices = $choices;
                    }else if($question->question_type_id == $question_types["Multiple Choice"]){
                        //Collect Child Questions

                        $children_questions = Question::on($project->database)->where(['parent'=>$question->id])->get();
                        $question->children_questions = $children_questions;

                        //Check if the question have choices 
                        $choices = Choice::on($project->database)->where(['question_id'=>$question->id])->lists("choice","choice_number");
                        $question->choices = $choices;
                        //dd($question->children_questions);
                    }else{
                        
                    }

                }

                //Set questions to each category 
                $category->questions = $questions;
                

            }
            
            $questions = Question::on($project->database)->get();
            








        }else{
            
        }
        //Trail here
        foreach ($categories as $category) {
            foreach ($category->questions as $question) {
                //dd($question);
            }
        }
        
        return view('surveys.survey',compact('categories','project'));





        

        
    }
    public function submit(Request $request,$survey_link){
        //dd($request->all());
        //Tying to stick the request 
        //TODO:: Validation here

        //If next is clicked

        //else if save is clicked
            //if values already in database update them

            //else if values already in 

        $survey = Survey::where(['link'=>$survey_link])->first();
        if(!empty($survey)){
            $project = Project::where(['id'=>$survey->project_id])->first();
            //Make a connection
            

            

            $this->configure_database($project->database);
            //Responses to the questions question_id => response
            $responses = $request->except('_token');
            
            //Get RespondentId from session 
            $surveys = $request->session()->get('interviewee.surveys');
            $respondent_id = $surveys[$survey->id];
            

            if(!empty($responses)){
                foreach($responses as $key => $value){
                    $response = array();
                    $response["created_at"] = Carbon::now();
                    $response["updated_at"] = Carbon::now();
                    $response["question_id"] = $key;
                    $response["response"] = $value;
                    $response["respondent_id"] = $respondent_id; //Temporary assignment
                    Response::on($project->database)->insert($response);
                    
                }
                $completed_surveys = $request->session()->get('interviewee.completed_surveys');
                $completed_surveys[$survey->id] = $respondent_id;
                $request->session()->set('interviewee.completed_surveys', $completed_surveys); 
                return redirect('/surveys/success');

            }else{
                return redirect()->back()->withInput();
            }


        }else{
            abort(404);
        }
        


        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_respondent(CreateRespondentRequest $request,$survey_link){

        $survey = Survey::where(['link'=>$survey_link])->first();
        if(!empty($survey)){
            $project = Project::where(['id'=>$survey->project_id])->first();
            //Make a connection
            //dd($request->all());
            $respondent = $request->except('_token','email');
            $respondent['created_at'] = Carbon::now();
            $respondent['updated_at'] = Carbon::now();
            //dd($request);
            //dd($respondent);
            
            $this->configure_database($project->database);
                
            //TODO :: Not working as expected
            
            DB::beginTransaction();

            try {
                $respondent_id = Respondent::on($project->database)->insertGetId($respondent);
                
                

                //Use this respondent id to Fill RespondentSegmentationValues
                // $segmentation_values = $request->except('first_name','last_name','_token','email');
                
                // foreach($segmentation_values as $segmentation_id => $value){
                //     $respondent_segmentation_value = array();
                //     $respondent_segmentation_value['created_at']  = Carbon::now();
                //     $respondent_segmentation_value['updated_at'] = Carbon::now();
                //     $respondent_segmentation_value['segmentation_id'] = $segmentation_id;
                //     $respondent_segmentation_value['respondent_id'] = $respondent_id;
                //     $respondent_segmentation_value['value'] = $value;
                    
                //     RespondentSegmentationValue::on($project->database)->insert($respondent_segmentation_value);    

                    



                // }
                
                    //Get the session array 
                    $surveys = $request->session()->get('interviewee.surveys');
                    $surveys[$survey->id] = $respondent_id;

                    //Set Session 
                    $request->session()->put('interviewee.surveys',$surveys);

                    //$request->session()->push('interviewee.surveys',($respondent_id => $survey->id));

                    //If successful set
                    return redirect("/surveys/".$survey_link);


                DB::commit();
                // all good
            } catch (Exception $e) {
                
                dd("Exception Handled");
                // something went wrong
            }
            
            //RespondentSegmentationValue::on($project->database)->
            //$request->session()->push('interviewer.surveys', $survey->id);


            
        }

        
    }
    public function register(Request $request, $survey_link)
    {
        //dd($request->all(),$survey_link);

        //Setup the get the segmentation values and Segmentations 
        $survey = Survey::where(['link'=>$survey_link])->first();
        //$request->session()->push('interviewee.surveys', $survey->id);
        if(!empty($survey)){

            //Check if the interviewee has already filled out basic information for this specific survey
            if(!empty($request->session()->get('interviewee.surveys'))){
                //User has filled out couple of times
                //dd($request->session()->get('interviewee'));

                if(array_search($survey->id, $request->session()->get('interviewee.surveys')) !== false){
                    //User has already submited the form
                    return redirect('/surveys/'.$survey_link);
                }


            }
            $project = Project::where(['id'=>$survey->project_id])->first();
            //Make a connection
            $this->configure_database($project->database);

            $segmentations = Segmentation::on($project->database)->get();
            $segmentation_value = array();
            foreach($segmentations as $segmentation){
                if(!empty($segmentation)){
                    $values = SegmentationValue::on($project->database)->whereSegmentationId($segmentation->id)->lists('value','id');

                    $segmentation->values = $values;

                }
            }
            //dd($segmentations->toArray());
            //dd($segmentation_value,$segmentations,$survey_link);
            return view('surveys.register',compact("segmentation_value","segmentations","survey_link"));
        }

        
        
        return view('surveys.register');
    }
    public function create()
    {

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function list_surveys(){
        $projects = Project::where(['published'=>1])->get(); // Temporary Id 1     
        //dd($projects);
        foreach ($projects as  $project) {
            $survey = Survey::where(['project_id'=> $project->id])->first();  
            if(!empty($survey)){
                $project->survey = $survey;    
            }
        }      
        $projects = $projects->toArray();

        return view('surveys.guest_index',compact("projects"));

    }
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $survey = Survey::where(['link'=>$survey_link])->first();
        if(!empty($survey)){
            $project = Project::where(['id'=>$survey->project_id])->first();
            //Make a connection
            $this->configure_database($project->database);

            
        }
        
    }
    /*
     * configure the database to be used
     *
     * @param string database
     */
    public function configure_database($database){
        Config::set("database.connections.$database", array(
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => "$database",
            'username'  => 'root',
            'password'  => '0000',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ));
    }
}
