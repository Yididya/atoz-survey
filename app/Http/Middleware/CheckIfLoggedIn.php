<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Project;


class CheckIfLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){

            //Check if that user owns the project
            $project_id = $request->id;
            $user_id = Auth::id();
            $project = Project::where(["user_id"=> $user_id,"id"=>$project_id])->get()->toArray();
            
            //Check if there exists no project matching the url
            if(empty($project)){
                //Send a flash message to the projects index page 
                return redirect('/projects');

            }
            
            return $next($request);
        }
        //dd("Hello");

        return redirect('/login');
        
        
    }
}
