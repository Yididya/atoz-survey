<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Project;


class CheckIfLoggedInBase
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            return $next($request);
        }
        //dd("Hello");

        return redirect('/login');
        
        
    }
}
