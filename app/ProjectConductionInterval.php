<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectConductionInterval extends Model
{
    protected $fillable =['interval','description'];
    protected $table = 'project_conduction_intervals';

    public function project_types(){
    	return $this->hasMany('App\ProjectType');
    }
}
