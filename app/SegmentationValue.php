<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SegmentationValue extends Model
{
    protected $fillable = ['segmentation_id','value'];
    protected $table ='segmentation_values';



    public function segmentation(){
    	return $this->belongsTo('App\Segmentation');
    }
}
