<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Segmentation extends Model
{
    protected $fillable = ['name','column_name','type',];
    protected $table ='segmentations';



    public function values(){
    	return $this->hasMany('App\SegmentationValue');
    }

    
    
}
