<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectType extends Model
{
    protected $fillable = ['type','description'];
    
    protected $table = 'project_types';
    

    public function interval(){
    	return $this->belongsTo('App\ProjectConductionInterval');
    }
    public function projects(){
    	return $this->hasMany('App\Project');
    }


}
