<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
    protected $fillabale = ['choice_number','choice','question_id'];

    protected $table = 'choices';
    
    
}
