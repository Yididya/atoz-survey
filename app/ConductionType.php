<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConductionType extends Model
{
    protected $fillabale = ['type','description'];

    protected $table = 'conduction_types';
    
    public function projects(){
        return $this->hasMany('App\Project');
    }
}
