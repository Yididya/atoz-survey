<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespondentSegmentationValue extends Model
{
    protected $fillable = ["segmentation_id","respondent_id","value"];
    protected $table = "respondent_segmentation_values";
}
