@extends('analysis.app')

@section('title')
	A-Z Survey | Analysis
@stop
@section('navigation_active_analysis')
 active
@stop
@section('content')
<style type="text/css">
	*{
		border-radius: 0 !important;
	}
	.project_list_item{
		background: #fff;
	}
	.search{
		margin-top: 23px;
		margin-bottom: 10px
	}
	#categories,#segmentations{
		width:initial;

	}
	#questions_headline{
		color:#2a6496;
		cursor:pointer;
	}
	#questions_body{
		display: none;
	}
	.chart{
		margin-top: 20px;
	}
	#respondent_filter_accordion{
		font-family: 'Lato'!important;
		font-size:14px;
	}

</style>
		<script type="text/javascript">
		$(document).ready(function(){
			




		});

		</script>

		<div class="a2z-jumbotron">
	        <div class="container">
	        		<div class="row">
	        			<div class="col-lg-6"><h1><a href="/home">A-Z </a>/ <a href="/analysis">Analysis</a> </h1></div>
	        			<div class="col-lg-6">
	        				
	        			</div>
	        			
	        		</div>
	                
	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				

				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="row">
								<div class="col-md-6">
									
								</div>
								<div class="col-md-6">
									<select class="form-control pull-right" id="categories">

				
										@foreach($categories as $value)

											@if($value->id == $category->id )
												<a href=""><option id="{{$value->id}}" selected>{{$value->category}}</option></a>
											@else 
												<option id="{{$value->id}}">{{$value->category}}</option>
											@endif
										
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<div class="panel panel-default">
								<div class="panel-heading" id="questions_headline">
									<p><strong> <i class="fa fa-chevron-right"></i> Showing Graph of : </strong> {{$category->questions->first()->question}}</p>
								</div>
								<div class="panel-body" id="questions_body">
									<table class="table table-stripped table-bordered questions_table" >
										<thead>
											<th>Question in Short</th>
											<th>Question</th>
										</thead>
										<tbody>
											@forelse($category->questions as $question)
												<tr>
													<td><a href="" id="question{{$question->id}}"class="question">{{$question->short_form}}</a> <span class="type" style="display:none">{{$question_types[$question->question_type_id]}}</span></td>
													<td>{{$question->question}}</td>
												</tr>
												
											@empty
											@endforelse
											
										</tbody>
									</table>


								</div>
							</div>
							<div class="child_questions row" style="display:none">

									<div class="container col-lg-11">
										<h4>Pick a child question below</h4>	
									</div>
									
									
								

							</div>
							<hr>
							<div class="row">
								<div class="col-md-6">
									<button class="btn btn-default" id="respondent_filter"><span class="respondent_filter_badge badge">0</span>Respondent's Filter <i class="fa fa-angle-down"></i></button>
									<button class="btn btn-success sync_changes">Sync Changes</button>
									
								</div>
								<div class="col-md-6">
									
									<!-- Segmentations goes here -->
									<select class="form-control pull-right" id="segmentations">
										@forelse($segmentations as $segmentation)
											<option value="{{$segmentation->id}}">{{$segmentation->name}}</option>
										@empty
										@endforelse
									</select>

								</div>
							</div>
							<div class="row">
								<div class="container">
									<div id="respondent_filter_accordion" class="col-lg-11"style="display:none">
										@forelse($segmentations as $segmentation)
											<p>{{ $segmentation->name }}</p>
											<div>
												@forelse($segmentation->values as $value)
													
													<label class="checkbox-inline">
														<input type="checkbox"  class="filter_checkbox" name="{{ $segmentation->id }}[]" class="filter_check" checked="" value="{{$value->value}}"> 															
												   		{{$value->value}}
												   		
													</label>	

												@empty
													<p>No values in this filter</p>
												@endforelse
												
											</div>
										@empty
										@endforelse
									</div>
								</div>
							</div>
							<!-- Chart goes here -->
							<div class="row chart">
								<div class="col-lg-9">
									<!-- Canvas goes here -->
									<div class="chart_container row" style="height : 500px">
										<canvas id="chart"></canvas>
									</div>		
								</div>
								<div class="col-lg-3">
									<!-- Legend goes here -->
									<div class="row">
										<h2>Legend </h2>		
									</div>
									
									
									<div class="legend">
										<!-- List of legends goes here -->
										<ul class="list-unstyled legend_list">
											
										</ul>
									</div>
									<div class="row">
										<h2>Export </h2>		
									</div>
									
									
									<div class="export">
										<!-- List of legends goes here -->
										<ul class="list-unstyled export_list">
											
										</ul>
									</div>
								</div>

							</div>


						</div>
					</div>
				</div>
				

			</div>
		</div>
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

@stop