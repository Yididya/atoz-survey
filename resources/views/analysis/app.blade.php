<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
    <meta name="csrf_token" content="{{ csrf_token() }}" />

	<link href="{!! asset('bootstrap/css/bootstrap.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
	<link href="{!! asset('font-awesome/css/font-awesome.css') !!}" media="all" rel="stylesheet" type="text/css" />
    <link href="{!! asset('jquery-ui/jquery-ui.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/lato.css') !!}" media="all" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="{!! asset('js/jquery-1.11.2.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('bootstrap/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/Chart.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/Chart.StackedBar.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/analysis.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('jquery-ui/jquery-ui.min.js') !!}"></script>
	<style type="text/css">
                .a2z-nav{
                    margin-bottom: 0px;
    	       }    
                
                .roboto{
                    font-family: 'Roboto';
                }
                body{
                    background-color: #fbfbfb;
                    font-family:'Lato';
                    
                }
                .header{
                    margin-bottom: 0px;
                }
                .body{
                    padding-top:80px;
                    padding-bottom: 80px;
                    /*background-color: #6f5499; 
                    color:#f0f0f0;*/
                }
                .btn-outline-inverse {
                    color: #fff;
                    background-color: transparent;
                    border-color: #cdbfe3;
                }
                .btn-outline-inverse:hover {
                    color: #010;
                    background-color: #f0f0f0;
                    border-color: #cdbfe3;
                }
                .title{
                    font-size:55px;
                }
                .a2z-icon{
                    cursor: pointer;
                    color:rgba(85, 85, 85, 0.81);
                    opacity:0.75;
                }
                .a2z-icon:hover{
                    opacity: 1;
                }
                .a2z-feature{
                    padding:80px 0px 50px;
                }
                .a2z-footer{
                    background-color: #DEDEDE;
                    padding-top:20px;
                }
                .a2z-header{
                    background-color: #6f5499; 
                    background-image: -webkit-gradient(linear, left top, left bottom, from(#563d7c), to(#6f5499)); 
                    background-image: -webkit-linear-gradient(top, #563d7c 0%, #6f5499 100%); 
                    background-image: -o-linear-gradient(top, #563d7c 0%, #6f5499 100%);
                    background-image: linear-gradient(to bottom, #563d7c 0%, #6f5499 100%);
                    background-repeat: repeat-x; 
                    color:#f0f0f0;
                    margin-bottom: 10px;
                }
	   </style>
</head>
<body>	
		<nav class="navbar navbar-inverse a2z-nav" data-offset-top="0">
            <div class="container navhigher">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">&nbsp;&nbsp;&nbsp;Survey Manager&nbsp;&nbsp;&nbsp;</a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#tabs" id="nav-collapser">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span> 
                </button>
            </div>
            <div id="tabs" class="collapse in">
                <ul class="nav navbar-nav navbar-right">
                    <li class="@yield('navigation_active_home') "><a href="/home"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;Home</a></li>
                    <li class="@yield('navigation_active_admin') " ><a href="/projects"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Projects</a></li>
                    <li class="@yield('navigation_active_survey') "><a href="/surveys"><span class="glyphicon glyphicon-zoom-in"></span>&nbsp;&nbsp;Surveys</a></li>
                    <li class="@yield('navigation_active_analysis') "><a href="/analysis"><i class="fa fa-bar-chart"></i>&nbsp;&nbsp;Analysis</a></li>
                    <li class="@yield('navigation_active_about')"><a href="/about"><i class="fa fa-info-circle"></i>&nbsp;&nbsp;About Us</a></li>
                </ul>
            </div>
            </div>
        </nav>

	
		@yield('content')

		
		<div class="a2z-footer">
        <div class="container">
                <div class="row">
                        <div class = "col-sm-6">
                                <p>&copy; Copyright Reserved 2015.</p>
                        </div>
                        <div class = "col-sm-6">
                                <ul class="pull-right list-inline">
                                        <li><i class="fa fa-facebook fa-2x a2z-icon"></i></li>
                                        <li><i class="fa fa-twitter fa-2x a2z-icon"></i></li>
                                        <li><i class="fa fa-google-plus fa-2x a2z-icon"></i></li>
                                </ul>
                        </div>
                </div>	
        </div>
</div>

</body>
</html>