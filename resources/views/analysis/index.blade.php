@extends('app')

@section('title')
	A-Z Survey | Analysis
@stop
@section('navigation_active_analysis')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}
	.search{
		margin-top: 23px;
		margin-bottom: 10px
	}

</style>
		<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    
			// $(".filter a").click(function(e){
			// 	e.preventDefault();

			// });
		});

		</script>

		<div class="a2z-jumbotron">
	        <div class="container">
	        		<div class="row">
	        			<div class="col-lg-6"><h1><a href="/home">A-Z </a>/ <a href="/projects">Analysis</a> </h1></div>
	        			<div class="col-lg-6">
	        				<form role="form">
	        					<input type="text" name ="q" placeholder="Search for surveys analysed here" class="form-control search" >
	        				</form>

	        			</div>
	        			
	        		</div>
	                
	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					<div class="panel panel-default">
						<div class="panel-heading"> Actions </div>
						<div class="panel-body">
							<ul class="list-unstyled">
								<li><a href="/projects/add">Add Project</a></li>
								<li><a href="">Edit Project</a></li>
								<li><a href="">Prepare survey</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-9">
					
							<div class="row">
								<div class="col-lg-10"><h2>Surveys </h2>	</div>
								
							</div>
							<hr>
							

							<div class="survey_list">
								@forelse($projects as $project)
									<ul class="list-unstyled">
									<li class="row project_list_item">
						                
						                    <div>
						                    	
							                        <div class="col-sm-12 col-lg-12">
							                                
							                                <h3>
							                                	@if($project["published"]== '1')
								                             		<a title="Link to the survey" href="/analysis/{{$project['survey']->link}}"><i class="fa fa-chain"></i> {{$project["title"]}}</a>
								                             	@else
							                                		<a href="/projects/{{$project['id']}}">{{$project["title"]}}</a>
							                                	@endif

							                             		
							                                	
							                                </h3>
							                             	<p>{{$project["description"] }}
							                             	<span class="pull-right">Date created :{{$project["created_at"]}} </span></p>
							                             	
							                             	

							                             
							                        </div>
							                        <div class="col-sm-3 col-lg-2">
							                            
							                        </div>
						                        
						                    </div>
						                
						            </li>

									</ul>    
									@empty
								    	<h3>No surveys to analyse yet</h3>
									@endforelse
								</div>



							
							
							


					
				</div>
				

			</div>
		</div>
		

@stop