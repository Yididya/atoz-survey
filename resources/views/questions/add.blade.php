<meta name="csrf-token" content="{{ csrf_token() }}" />
@extends('app')

@section('title')
	A-Z Survey | Projects
@stop
@section('navigation_active_admin')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}


</style>
		<div class="a2z-jumbotron">
	        <div class="container">
	                <h1><a href="">{{$project->title}}</a> / <a href="">Add Question</a></h1>

	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					@include("sidebar")
				</div>

				<div class="col-md-9">
					
							<h2>Add a new question</h2>
							<hr>
							
								@if(count($errors) >0 )
		                            <div class="alert alert-danger">
		                                <strong>Oops!</strong> There were some problem with your input
		                                <ul>
		                                    @foreach($errors->all() as $error)
		                                        <li>{{ $error }}</li>
		                                    @endforeach
		                                </ul>   
		                            </div>
		                        @endif
								{!! Form::open(array('url' => '/projects/'.$project->id.'/questions/add', 'class' => 'form')) !!}
								<div class="form-group">
								    {!! Form::label('Question') !!}
								    {!! Form::text('question', null, 
								        array('required', 
								              'class'=>'form-control', 
								              'placeholder'=>'Question')) !!} 
								</div>
								<div class="form-group">
								    {!! Form::label('Short Form') !!}
								    {!! Form::text('short_form', null, 
								        array('required', 
								              'class'=>'form-control', 
								              'placeholder'=>'Short Form')) !!} 
								</div>
								<div class="form-group">
								    {!! Form::label('Question Type') !!}
								    {!!Form::select('question_type_id', $question_types, null, ['id' => 'question_type','class'=>'form-control select','placeholder' => 'Pick a question type'])!!}
								</div>
								<div class="form-group">
								    {!! Form::label('Category') !!}
								    {!!Form::select('category_id', $categories, null, ['id'=>'categories','class'=>'form-control select','placeholder' => 'Pick a a category'])!!}
								</div>
								
								<div class="form-group">
								    {!! Form::label('Parnet Questions') !!}
								    {!!Form::select('parent', [] , null, ['id'=>'parent','class'=>'form-control select','placeholder' => 'Pick a question type'])!!}
								</div>
								<!-- Single Choice Question Choices  -->

								<!-- if(singleChoice is choosen) -->
								<div id="single_choices" style ="display:none">
									<div class="row form-group">
										<div class="col-lg-1"><strong>+/-</strong></div>
										<div class="col-lg-11"><strong>Choices</strong></div>
									</div>

									<div class="form-group row" id="choice_0" style="display:none;">
											
												<div class="col-lg-1">
													<button title="click to add/remove the row" type="button" class="btn btn-xs btn-default"><i class='fa fa-minus-circle alert-danger'></i></button>	
												</div>
												<div class="col-lg-11">
													<input type="text" class="form-control" name="choice_values" >
												</div>
												<br>
											
									</div>

									<div class="form-group" id="add_choice">
											<button title="click to add/remove the row" type="button" class="btn btn-xs btn-default"><i class="fa fa-plus-circle alert-success"></i></button>	
											
									</div>
								</div>
								<!-- End Choice question Choices -->

									

									<br>
								
								
								<script type="text/javascript">
									var lastRow=0;
										$("#add_choice button").click(function(){
											
											addChoice();
										});

										function removeChoice(x) {
											$("#choice_"+x).remove();
										}	
										function addChoice() {
											lastRow++;
											$("#choice_0").clone(true).attr('id','choice_'+lastRow).removeAttr('style').insertBefore("#add_choice").attr('required',true);
											$("#choice_"+lastRow+" button").attr('onclick','removeChoice('+lastRow+')');
											$("#choice_"+lastRow+" input:first").attr('name','choice_values[]').attr('id','choice_values_'+lastRow);
										}



								</script>

								<script type='text/javascript'>
									$(document).ready(function(){
										$.ajaxSetup({
									        headers: {
									            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
									        }
									    });
										//Set the selected prop to false
										//$(".select option").prop("selected",false);
										

										$("#question_type").change(function(){
											var question_type = $("#question_type option[selected]").html();
											//console.log(question_type);
											
											//If the question type is single
											if($("#question_type option[value=4]").prop("selected")){
												if($("#parent option:selected").val() == 0){
													$("#single_choices").show();
												}else{
													$("#single_choices").hide();
												}
												
											}else if($("#question_type option:selected").html() == "Multiple Choice"){

												if($("#parent option:selected").val() == 0){
													$("#single_choices").show();
												}else{
													$("#single_choices").hide();
												}
												//Remove the parent
											}

										});
										
										$("#parent").change(function(){
											if($("#question_type option[value=4]").prop("selected")){
												if($("#parent option:selected").val() == 0){
													$("#single_choices").show();
												}else{
													$("#single_choices").hide();
												}
												
											}
										});

										$("#categories").change(function(){
											var category_id = $("#categories option:selected").val();
											
											$.post( "./load_multiple_questions", { category : category_id },function(data){
												$("#parent").html("<option value=0>Pick a parent question</option>");

												for(var question in data){
													$("#parent").append("<option value="+ question+ ">"+ data[question] +"</option>");
												}
											},"json");
												  
										});

										

										
									});
										
										

									
									
								</script>



								<div class="form-group">
								    {!! Form::submit('Add Question', 
								      array('class'=>'btn btn-success')) !!}
								</div>
								{!!Form::close()!!}
							

					
				</div>
				

			</div>
		</div>
		

@stop