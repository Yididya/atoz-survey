@extends('app')


@section('content')

	<div class="a2z-jumbotron">
	        <div class="container">
	                <h1> <a href="/projects/{{$project->id}}">{{$project->title}}</a> / <a href="">Questions </a></h1>
	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					@include("sidebar")
				</div>

				<div class="col-md-9">
					
							<h2>Questions</h2>
							<hr>
							
							
								<table class="table">
									<thead>
										<tr>
											<th>Question</th>
											<th>Short form</th>
											<th>Type</th>
											<th>Category</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										@forelse($questions as $question)
										<tr>
											<td><a href="/projects/{{$project->id}}/questions/{{$question->id}}">{{$question->question}}</a></td>	
											<td>{{$question->short_form}}</td>
											<td>{{$question->question_type_id}}</td>
											<td>{{$question->category_id}}</td>
											<td>
												<a href="" title = "Delete">
													<i class="fa fa-trash"></i> 
												</a>
												<a href="" title = "Edit">
													<i class="fa fa-pencil"></i> 
												</a>
											</td>
										</tr>
									</tbody>
									    @empty
							    			<h3>No questions yet</h3>
										@endforelse

								</table>
								{!! $questions->render() !!}
								

							


							
							
							


					
				</div>
				

			</div>
		</div>


@stop