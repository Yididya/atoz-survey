@extends('app')

@section('title')
	A-Z Survey | Projects
@stop
@section('navigation_active_admin')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}

</style>
		<div class="a2z-jumbotron">
	        <div class="container">
	                <h1>Add Projects</h1>

	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					@include("sidebar")
				</div>

				<div class="col-md-9">
					
							<h2>Add a new question</h2>
							<hr>
							
								@if(count($errors) >0 )
		                            <div class="alert alert-danger">
		                                <strong>Oops!</strong> There were some problem with your input
		                                <ul>
		                                    @foreach($errors->all() as $error)
		                                        <li>{{ $error }}</li>
		                                    @endforeach
		                                </ul>   
		                            </div>
		                        @endif
								{!! Form::open(array('url' => '/projects'.$project->id.'/questions/add', 'class' => 'form')) !!}
								<div class="form-group">
								    {!! Form::label('Question') !!}
								    {!! Form::text('question', null, 
								        array('required', 
								              'class'=>'form-control', 
								              'placeholder'=>'Question')) !!} 
								</div>
								<div class="form-group">
								    {!! Form::label('Short Form') !!}
								    {!! Form::text('short_form', null, 
								        array('required', 
								              'class'=>'form-control', 
								              'placeholder'=>'Short Form')) !!} 
								</div>
								<div class="form-group">
								    {!! Form::label('Question Type') !!}
								    {!!Form::select('project_type_id', $question_type, null, ['class'=>'form-control','placeholder' => 'Pick a question type'])!!}
								</div>
								

								<div class="form-group">
								    {!! Form::submit('Add Project', 
								      array('class'=>'btn btn-success')) !!}
								</div>
								{!!Form::close()!!}
							

					
				</div>
				

			</div>
		</div>
		

@stop