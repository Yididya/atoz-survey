<div class="action_lists card well">
				        <div>
				            <ul class="nav nav-list">
				                <li><label class="tree-toggle nav-header">Projects</label>
				                    <ul class="nav nav-list tree">
				                        <li><a href="/projects/add">Add a Project</a></li>
				                        <li><a href="/projects">View Projects</a></li>
				                    </ul>
				                </li>
				                <li class="divider"></li>
				                <hr>
				                <li><label class="tree-toggle nav-header">Categories</label>
				                    <ul class="nav nav-list tree">
				                        <li><a href="/projects/{{$project->id}}/categories/add">Add a Questions Category</a></li>
				                        <li><a href="/projects/{{$project->id}}/categories">View Categories</a></li>
				                    </ul>
				                </li>
				                <li class="divider"></li>
				                <hr>
				                <li><label class="tree-toggle nav-header">Questions</label>
				                    <ul class="nav nav-list tree">
				                        <li><a href="/projects/{{$project->id}}/questions/add">Add a Question</a></li>
				                        <li><a href="/projects/{{$project->id}}/questions">View Questions</a></li>
				                    </ul>
				                </li>
				                <li><label class="tree-toggle nav-header">Segmentations</label>
				                    <ul class="nav nav-list tree">
				                        <li><a href="/projects/{{$project->id}}/segmentations/add">Add a Segmentations</a></li>
				                        <li><a href="/projects/{{$project->id}}/segmentations">View Segmentation</a></li>
				                    </ul>
				                </li>
				            </ul>
				        </div>
				    </div>