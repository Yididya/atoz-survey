@extends('app')


@section('content')
<div class="a2z-jumbotron">
            <div class="container">
                    <h1><a href="/home">A-Z </a>/ Register </h1>
            </div>
</div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        @if(count($errors) >0)
                            <div class="alert alert-danger">
                                <strong>Oops!</strong> There were some problem with your input
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>   
                            </div>
                        @endif

                    <form class="form-horizontal" role="form" method="POST" action="/auth/register">
                        <input  type="hidden" name="_token" value ="{{ csrf_token()}}">
                        
                        <div class="form-group">

                            <label class="col-md-4 control-label">First Name</label>
                            <div class="col-md-6">
                                <input type="text" name="first_name" class="form-control" value="{{ old('first_name')}}">
                            </div>
                        </div>  

                        <div class="form-group">
                            <label class="col-md-4 control-label">Last Name</label>
                            <div class="col-md-6">
                                <input type="text" name="last_name" class="form-control" value="{{ old('last_name')}}">
                            </div>
                        </div>  

                        <div class="form-group">
                            <label class="col-md-4 control-label">User Name</label>
                            <div class="col-md-6">
                                <input type="text" name="username" class="form-control" value="{{ old('username')}}">
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-md-4 control-label">email</label>
                            <div class="col-md-6">
                                <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-md-4 control-label">Phone</label>
                            <div class="col-md-6">
                                <input type="phone" name="phone_number" class="form-control" value ="{{old('phone_number')}}" placeholder="eg. 0935924544">
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input type="password" name="password" class="form-control">
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input type="password" name="password_confirmation" class="form-control">
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <input type="submit" value="Register" class="btn btn-success">
                            </div>
                        </div>
                    </form>
                        



                    </div>
                </div>  
            </div>
            
        </div>
    </div>
@stop
