@extends('app')

@section('title')
    A-Z Survey | Login
@stop
@section('navigation_active_user')
 active
@stop

@section('content')
<style type="text/css">
    .login_body{
        padding:100px 0;
    }
    #loginForm{
        position: relative;
    }
</style>
<div class="a2z-jumbotron">
            <div class="container">
                    <h1><a href="/home">A-Z </a>/ <a href="/auth/login">Login</a> </h1>
            </div>
</div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="login_body">

                    <form class="form-horizontal" role="form" method="POST" action="/auth/login" id="loginForm">
                                            
                        @if(count($errors) >0)
                            <div class="alert alert-danger">
                                <strong>Oops!</strong> There were some problem with your input
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>   
                            </div>
                            <script type="text/javascript">
                            $(document).ready(function(){

                                $( "#loginForm" ).animate({
                                    
                                    left:"100px",
                                    right:"0px"
                                  }, 200).animate({
                                    left:"0px",
                                    right:"100px"
                                  },200).animate({
                                    left:"100px",
                                    right:"0px"
                                  },200).animate({
                                    left:"0px",
                                    right:"0px"},200);

                            });

                            </script>
                        @endif

                        <div class="text-center"> <i class="fa fa-lock fa-5x a2z-icon"></i></div>
                        <input  type="hidden" name="_token" value ="{{ csrf_token()}}">
                        
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label">Email</label>
                            <div class="col-md-6">
                                <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                            </div>
                        </div>  
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input type="password" name="password" class="form-control">
                            </div>
                        </div>  
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <input type="submit" value="Login" class="btn btn-success">
                            </div>
                        </div>
                    </form>
                        



                    </div>
                </div>  
            </div>
            
        </div>
    </div>
@stop
