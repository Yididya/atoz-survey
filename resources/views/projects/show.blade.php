@extends('app')

@section('title')
	A-Z Survey | Projects
@stop
@section('navigation_active_admin')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}
	.card-description{
		position: relative;
	    background-color: #f0f0f0;
	    border-top: 1px solid rgba(160,160,160,0.2);
	    padding-bottom: 10px;
	    z-index: 2;
	}
	.card-icon{
		padding:20px 0;
	}

</style>
		<div class="a2z-jumbotron">
	        <div class="container">
	                <h1><a href="/home">A-Z </a>/ <a href="/projects">My Projects</a> / <a href="/projects/{{$project->id}}">{{$project->title}}</a></h1>
	        </div>
		</div>


		
		<div class="container">
			<div class="row text-center"> 
				<h1>Project Dashboard</h1>
			</div>
			<hr/>
		</div>


		<div class="container" style="margin-top:20px">
			<div class="row">
				<div class="col-md-4"> 
					
						
						
						<div class="card text-center" style="padding:10px 0 0">
							<a href="/projects/{{$project->id}}/segmentations"><img class="card-icon" src="{!! asset('img/filter.png') !!}" width="140"  /></a>	
							<div class="card-description">
								<h3>
									Filters[{{$project->segmentations_count}}]

								</h3>
								
								<a href="/projects/{{$project->id}}/segmentations/add">Add</a> | <a href="/projects/{{$project->id}}/segmentations">View</a>

							</div>
							
						</div>
					
				</div>
				<div class="col-md-4"> 
					
						
						<div class="card text-center" style="padding:10px 0 0">
							<a href="/projects/{{$project->id}}/categories"><img class="card-icon" src="{!! asset('img/projectIcon2.png') !!}" width="140"  /></a>	
							<div class="card-description">
									<h3>
									Categories[{{$project->categories_count}}]

								</h3>
								
								<a href="/projects/{{$project->id}}/categories/add">Add</a> | <a href="/projects/{{$project->id}}/categories">View</a>
							</div>
						</div>
					
				</div>
				<div class="col-md-4"> 
					
					
						<div class="card text-center" style="padding:10px 0 0">
							<a href="/projects/{{$project->id}}/questions"><img class="card-icon" src="{!! asset('img/projectIcon.png') !!}" width="140"  /></a>	
							<div class="card-description">
							    <h3>
									Questions[{{$project->questions_count}}]

								</h3>
								
								<a href="/projects/{{$project->id}}/questions/add">Add</a> | <a href="/projects/{{$project->id}}/questions">View</a>
							</div>
						</div>
				</div>

			</div>
		</div>

		<div class="container">
			<div class="row text-center"> 
				<h1>Project Description</h1>
			</div>
			<hr/>
		</div>
		
		<div class="container">
			<div class="row">
				

				<div class="col-md-12">
					
							<h2>Project Title</h2>
							<p>{{$project->title}}</p>
							<h2>Description</h2>
							<p>{{$project->description}}</p>
							<h2>Created At</h2>
							<p>{{$project->created_at->toFormattedDateString()}}</p>
							<h2>Author</h2>
							<p>{{$project->user->first_name}},{{$project->user->last_name}}</p>
							
							
							
				</div>
				

			</div>
		</div>
		

@stop