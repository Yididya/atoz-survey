@extends('app')

@section('title')
	A-Z Survey | Projects
@stop
@section('navigation_active_admin')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}
	.action_lists{
		margin-top: 20px;

	}

</style>
		<div class="a2z-jumbotron">
	        <div class="container">
	                <h1><a href="/">A-Z</a> / <a href="/projects"> Project </a> / Add</h1>

	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					<div class="well action_lists card">
				        <div>
				            <ul class="nav nav-list">
				                <li><label class="tree-toggle nav-header">Projects</label>
				                    <ul class="nav nav-list tree">
				                        <li><a href="/projects/add">Add a Project</a></li>
				                        <li><a href="/projects">View Projects</a></li>
				                    </ul>
				                </li>
				                <li class="divider"></li>
				                <hr>
				         	</ul>
				         </div>
				     </div>
					
				</div>

				<div class="col-md-9">
					
							<h2>Add a new project</h2>
							<hr>
							
								@if(count($errors) >0)
		                            <div class="alert alert-danger">
		                                <strong>Oops!</strong> There were some problem with your input
		                                <ul>
		                                    @foreach($errors->all() as $error)
		                                        <li>{{ $error }}</li>
		                                    @endforeach
		                                </ul>   
		                            </div>
		                        @endif
		                        @if(session('database_name_error'))
		                        	<div class="alert alert-danger">
		                                <strong>Oops!</strong> There were some problem with your input
		                                <ul>
		                                    {{ session('database_name_error') }}
		                                </ul>   
		                            </div>
		                        @endif
								{!! Form::open(array('url' => '/projects/add', 'class' => 'form')) !!}
								<div class="form-group">
								    {!! Form::label('Project Title') !!}
								    {!! Form::text('title', null, 
								        array('required', 
								              'class'=>'form-control', 
								              'placeholder'=>'Project Title',
								              'value'=>old('title'))) !!} 
								</div>

								<div class="form-group">
								    {!! Form::label('Project Type') !!}
								    {!!Form::select('project_type_id', $project_type, null, ['class'=>'form-control','placeholder' => 'Pick project type....'])!!}
								</div>
								
								<div class="form-group">
								    {!! Form::label('Conduction Type') !!}
								    {!!Form::select('conduction_type_id', $conduction_type, null, ['class'=>'form-control','placeholder' => 'Pick a conduction type...'])!!}
								</div>
								<div class="form-group">
								    {!! Form::label('Project Description') !!}
								    {!! Form::textarea('description', null, 
								        array('required', 
								              'class'=>'form-control', 
								              'placeholder'=>'Project Description',
								              'value'=>old('description')
								              )) !!}
								</div>

								<div class="form-group">
								    {!! Form::submit('Add Project!', 
								      array('class'=>'btn btn-success')) !!}
								</div>
								{!!Form::close()!!}
							

					
				</div>
				

			</div>
		</div>
		

@stop