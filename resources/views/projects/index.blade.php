@extends('app')

@section('title')
	A-Z Survey | Projects
@stop
@section('navigation_active_admin')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}
	

</style>
		<script type="text/javascript">
		$(document).ready(function(){
			
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
			$(".delete").click(function(){
				//Get the project Id 
				var project_id = $(this).next().text();


				$.ajax({
					type:"GET",
					url:"/project/detail/"+project_id,
					
					success : function(project){
						//Bind data to the Modal
						console.log(project);
						$("#project_id").text(project["id"]);
						$("#project_categories").text(project["categories_count"]);
						$("#project_questions").text(project["questions_count"]);

					},
					dataType:"json"
				});
				$('#myModal').modal('show');


			});
			$(".confirm_delete").click(function(){
				//alert("adfa");
				$.ajax({
					type:"GET",
					url:"/project/delete/"+ $("#project_id").text(),
					//data:{"_token":$('input[name="_token"]').attr('value'),"project_id" : $("#project_id").text()},
					success : function(result){
						console.log("Hello");
						$("#myModal").modal('hide');
						$("#list_item_"+$("#project_id").text()).fadeOut(1000,function(){
							$(this).remove();	
						});

						
					},
					dataType:"json"
				});
				
			});
		});

		</script>

		<div class="a2z-jumbotron">
	        <div class="container">
	                <h1><a href="/home">A-Z </a>/ <a href="/projects">My Projects</a> </h1>
	        </div>
		</div>
		<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Confirmation</h4>
		      </div>
		      <div class="modal-body">
		        <p>You are about to delete a project</p>
		        <ul>
		        	<li style="display:none"><span id="project_id"></span></li>
		        	<li>It has <span id="project_categories"></span> categories </li>
		        	<li>It has <span id="project_questions"></span> questions </li>
		        </ul>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary confirm_delete">Confirm</button>
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					<div class="well action_lists card">
				        <div>
				            <ul class="nav nav-list">
				                <li><label class="tree-toggle nav-header">Projects</label>
				                    <ul class="nav nav-list tree">
				                        <li><a href="/projects/add">Add a Project</a></li>
				                        <li><a href="/projects">View Projects</a></li>

				                    </ul>
				                </li>
				                <li class="divider"></li>
				                <hr>
				                <li><a href="/projects">Trash</a></li>
				                
				         	</ul>
				         </div>
				     </div>
				     
					
				</div>

				<div class="col-md-9">

							@if(session('user'))
								<div class="alert alert-success">
									<h1>Hello {{session('user')["first_name"]}}, You have successfully been registered</h1>
									<p>This section is where you build your surveys </p>
								</div>
							@endif
							
							<h2>Projects</h2>
							<hr>
							
							@forelse($projects as $project)
								<ul class="list-unstyled">
								<li class="row project_list_item" id="list_item_{{$project->id}}">
					                
					                    <div>
					                    	
						                        <div class="col-sm-12 col-lg-12">
						                                
						                                <h3>

						                                	<a href="/projects/{{$project->id}}">{{$project->title}}</a>
						                                	
						                                	<a href="javascript:void(0)" class="pull-right" title="Edit project"><i class="fa fa-pencil"></i> </a>
						                                	<a href="javascript:void(0)" class="delete pull-right" title="Delete project" role="button" > <i class="fa fa-trash"></i> </a>
						                                	<span class="project_id" style="display:none">{{$project->id}}</span>
						                                </h3>
						                             	<p>{{$project->description }}<span class="pull-right">Date created :{{$project->created_at}}</span></p>
						                             
						                        </div>
						                        <div class="col-sm-3 col-lg-2">
						                            
						                        </div>
					                        
					                    </div>
					                
					            </li>

								</ul>    
								@empty
							    	<h3>No projects yet</h3>
								@endforelse
								{!! $projects->render() !!}



							
							
							


					
				</div>
				


			
		</div>
		</div>

		

@stop