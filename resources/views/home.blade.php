@extends('app')

@section('title')
	A-Z survey | Home
@stop
@section('navigation_active_home')
	active
@stop
@section('content')
	<div class="body a2z-jumbotron">
        <div class="container text-center">
                <h1 class="title">A-Z Survey Master</h1>
                <p class="lead"> A-Z Survey Master is the the most popular multi purpose Survey Tool in the market.</p>
                <p class="lead"><a href="/about" class="btn btn-lg btn-outline-inverse">Learn More</a> <a href="/register" class="btn btn-lg btn-outline-inverse">Register</a></p>
        </div>
	</div>
	<div class="container a2z-feature">
	        <div class="row text-center">
	                <h1>A-Z  &nbsp; All In One</h1>
	                <br><br>
	        </div>
	        <div class="row text-center">
	                <div class="col-lg-4 ">
	                        <a href="projects"><i class="fa fa-pencil fa-5x a2z-icon"></i></a>
	                        <h3>Prepare your survey</h3>
	                        <p>A-Z allows you prepare your survey yourself</p>
	                </div>
	                <div class="col-lg-4">
	                        <a href="surveys"><i class="fa fa-share-alt fa-5x a2z-icon"></i></a>
	                        <h3>Share link to interviewees </h3>
	                        <p>Share link of your survey to interviewees and gather responses</p>
	                </div>
	                <div class="col-lg-4">
	                        <a href="analysis"><i class="fa fa-bar-chart fa-5x a2z-icon"></i></a>
	                        <h3>Analyse results</h3>
	                        <p>A-Z analyses responses from interviewees</p>
	                </div>
	        </div>
	        <hr>
	</div>

@stop