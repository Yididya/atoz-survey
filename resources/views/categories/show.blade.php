@extends('app')

@section('title')
	A-Z Survey | Categories
@stop
@section('navigation_active_admin')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}

</style>
		<div class="a2z-jumbotron">
	        <div class="container">
	                 <h1><a href="/projects/{{$project->id}}">{{$project->title}}</a> / <a href="/projects/{{$project->id}}/categories">Question Categories</a></h1>
	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					@include("sidebar")
				</div>

				<div class="col-md-9">
					
							<h2>Category</h2>
							<p>{{$category->category}}</p>
							<h1>Question list</h1>
							<table class="table">
									<thead>
										<tr>
											<th>Question</th>
											<th>Short form</th>
											<th>Type</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
									@forelse($questions as $question)
										<tr>
													<td><a href="/projects/{{$project->id}}/questions/{{$question->id}}">{{$question->question}}</a></td>	
													<td>{{$question->short_form}}</td>
													<td>{{$question->question_type_id}}</td>
													
													<td>
														<a href="" title = "Delete">
															<i class="fa fa-trash"></i> 
														</a>
														<a href="" title = "Edit">
															<i class="fa fa-pencil"></i> 
														</a>
													</td>
												</tr>
											
									@empty
										<h3>No Question yet, To add a question click on "Add Questions to Category" from side bar </h3>	
									@endforelse
									</tbody>
									</table>
							
				</div>
				

			</div>
		</div>
		

@stop