@extends('app')

@section('title')
	A-Z Survey | Respondents
@stop
@section('navigation_active_admin')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}

</style>
		<div class="a2z-jumbotron">
	        <div class="container">
	                <h1><a href="/projects/{{$project->id}}">{{$project->title}}</a> / Add Category</h1>

	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					@include("sidebar")
				</div>

				<div class="col-md-9">
					
							<h2>Add a new category</h2>
							<hr>
							
							@if(count($errors) > 0)
	                            <div class="alert alert-danger">
	                                <strong>Oops!</strong> There were some problem with your input
	                                <ul>
	                                    @foreach($errors->all() as $error)
	                                        <li>{{ $error }}</li>
	                                    @endforeach
	                                </ul>   
	                            </div>
	                        @endif
							{!! Form::open(array('url' => '/projects/'.$project->id.'/categories/add', 'class' => 'form')) !!}
								<div class="form-group">
								    {!! Form::label('Question Category') !!}
								    {!! Form::text('category', null, 
								        array('required', 
								              'class'=>'form-control', 
								              'placeholder'=>'Category')) 
								    !!} 
								</div>
								
								
								
								<div class="form-group">
								    {!! Form::submit('Add Category!', 
								      array('class'=>'btn btn-success')) !!}
								</div>
								{!!Form::close()!!}
							

					
				</div>
				

			</div>

		</div>
			

@stop