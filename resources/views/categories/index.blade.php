@extends('app')

@section('title')
	A-Z Survey | Projects
@stop
@section('navigation_active_admin')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}

</style>
		<div class="a2z-jumbotron">
	        <div class="container">
	                <h1> <a href="/projects/{{$project->id}}">{{$project->title}}</a> / <a href="">Question Categories</a></h1>
	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					@include("sidebar")
				</div>

				<div class="col-md-9">
					
							<h2>Categories</h2>
							<hr>
							
							@forelse($categories as $category)
								<ul class="list-unstyled">
								<li class="row project_list_item">
					                
					                    <div>
					                    	
						                        <div class="col-sm-12 col-lg-12">
						                                
						                                <h3>
						                                	<a href="/projects/{{$project->id}}/categories/{{$category->id}}">{{$category->category}}</a>
						                                	
						                                	<a href="" class="pull-right" title="Edit project"><i class="fa fa-pencil"></i> </a>
						                                	<a href="" class="pull-right" title="Delete project"> <i class="fa fa-trash"></i> </a>
						                                </h3>
						                             	<p><span class="pull-right">Date created :{{$category->created_at}}</span></p>
						                             
						                        </div>
						                        <div class="col-sm-3 col-lg-2">
						                            
						                        </div>
					                        
					                    </div>
					                
					            </li>

								</ul>    
								@empty
							    	<h3>No categories yet</h3>
								@endforelse

							


							
							
							


					
				</div>
				

			</div>
		</div>
		

@stop