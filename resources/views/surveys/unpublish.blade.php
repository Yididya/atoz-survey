<meta name="csrf-token" content="{{ csrf_token() }}" />
@extends('app')

@section('title')
	A-Z Survey | Projects
@stop
@section('navigation_active_survey')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}
	.search{
		margin-top: 23px;
		margin-bottom: 10px
	}
	.content{
		margin-top: 20px;
	}
	
</style>
		<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    
			$("#confirm").click(function(){

				$.post("",function(data){
					if(data == 200){
						$(".confirmation_dialog").hide();
						$(".flash_message").show();
						$(".flash_message").addClass("alert alert-success").html("<strong>Successful ! </strong>You have successfully unpublished a project");
						
					}
				});
			});
		});

		</script>


		<div class="a2z-jumbotron">
	        <div class="container">
	        		<div class="row">
	        			<div class="col-lg-6"><h1><a href="/home">A-Z </a>/ <a href="/projects">Surveys</a> </h1></div>
	        			<div class="col-lg-6">
	        				<form role="form">
	        					<input type="text" name ="q" placeholder="Search for surveys" class="form-control search" >
	        				</form>

	        			</div>
	        			
	        		</div>
	                
	        </div>
		</div>
		
		<div class="container">
			<div class="row content">
				
				<div class="col-md-3">
					<div class="well action_lists">
				        <div>
				            <ul class="nav nav-list">
				                <li><label class="tree-toggle nav-header">Projects</label>
				                    <ul class="nav nav-list tree">
				                        <li><a href="/projects/add">Add a Project</a></li>
				                        <li><a href="/projects">View Projects</a></li>
				                    </ul>
				                </li>
				                <li class="divider"></li>
				                <hr>
				         	</ul>
				         </div>
				     </div>
				     
				</div>

				<div class="col-md-9">
					<div class="flash_message"></div>
					

					<div class="confirmation_dialog">
						<h1>Confirm Action</h1>
						<p>Are you sure you want to unpublish this project? The project has {{$category_count}} categories and {{$question_count}} questions ?</p>
						<div class="form-group">
							<input type="button" class="btn btn-success" value="Confirm" id="confirm">
						</div>
					</div>
					<div><a href="/surveys">Back to Surveys</a></div>
					


							
							
							


					
				</div>
				

			</div>
		</div>
		

@stop