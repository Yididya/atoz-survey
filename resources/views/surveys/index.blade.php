@extends('app')

@section('title')
	A-Z Survey | Projects
@stop
@section('navigation_active_survey')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
		box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
	}
	.search{
		margin-top: 23px;
		margin-bottom: 10px
	}

</style>
		<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    
			// $(".filter a").click(function(e){
			// 	e.preventDefault();

			// });
		});

		</script>

		<div class="a2z-jumbotron">
	        <div class="container">
	        		<div class="row">
	        			<div class="col-lg-6"><h1><a href="/home">A-Z </a>/ <a href="/projects">Surveys</a> </h1></div>
	        			<div class="col-lg-6">
	        				<form role="form" method="POST" action="">
                        	<input  type="hidden" name="_token" value ="{{ csrf_token()}}">
	        					@if(isset($query['q']))
		        					<input type="text" name ="q" placeholder="Search for surveys" class="form-control search" required value="{{ $query['q']}}" >
	        					@else
	        						<input type="text" name ="q" placeholder="Search for surveys" class="form-control search" required >
	        					@endif
	        				</form>

	        			</div>
	        			
	        		</div>
	                
	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					<div class="panel panel-default">
						<div class="panel-heading"> Actions </div>
						<div class="panel-body">
							<ul class="list-unstyled">
								<li><a href="/projects/add">Add Project</a></li>
								<li><a href="">Edit Project</a></li>
								<li><a href="">Prepare survey</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-md-9">
					
							<div class="row">
								<div class="col-lg-12"><h2>Surveys </h2>	

									@if(Session::has('message'))
							        <div class="alert alert-success">
							            <h2>{{ Session::get('message') }}</h2>
							        </div>
							    @endif
								</div>
								
								
								
							</div>
							<hr>
							<div class="row ">
								<div class="dropdown pull-right">
								    <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Filter <i class="fa fa-filter"></i>
								    <span class="caret"></span></button>
								    <ul class="dropdown-menu filter" role="menu" aria-labelledby="menu1">
								      <li role="presentation"><a role="menuitem" tabindex="-1" href="/surveys">All</a></li>
								      <li role="presentation"><a role="menuitem" tabindex="-1" href="/surveys/published">Published</a></li>
								      <li role="presentation"><a role="menuitem" tabindex="-1" href="/surveys/not_published">Not yet published</a></li>
								      <li role="presentation" class="divider"></li>
								      
								    </ul>
							  	</div>
							</div>

							<div class="survey_list">
								@forelse($projects as $project)
									<ul class="list-unstyled">
									<li class="row project_list_item">
						                
						                    <div>
						                    	
							                        <div class="col-sm-12 col-lg-12">
							                                
							                                <h3>
							                                	@if($project->published == '1')
								                             		<a title="Link to the survey" href="/surveys/{{$project->survey->link}}"><i class="fa fa-chain"></i> {{$project->title}}</a>
								                             	@else
							                                		<a href="/projects/{{$project->id}}">{{$project->title}}</a>
							                                	@endif

							                             		
							                                	<a href="" class="pull-right" title="Edit project"><i class="fa fa-pencil"></i> </a>
							                                	<a href="" class="pull-right" title="Delete project"> <i class="fa fa-trash"></i> </a>
							                                </h3>
							                             	<p>{{$project->description }}
							                             	<span class="pull-right">Date created :{{$project->created_at}} </span></p>
							                             	
							                             	@if($project->published == 0)
							                             		<span class="pull-right"><a href="/surveys/{{$project->id}}/publish">Publish</a></span>
							                             	@else
							                             		<span class="pull-right"><a href="/surveys/{{$project->id}}/unpublish">Unpublish</a></span>
							                             	@endif
							                             	

							                             
							                        </div>
							                        <div class="col-sm-3 col-lg-2">
							                            
							                        </div>
						                        
						                    </div>
						                
						            </li>

									</ul>    
									@empty
								    	<h3>No surveys yet</h3>
									@endforelse
								</div>
								{!! $projects->render() !!}



							
							
							


					
				</div>
				

			</div>
		</div>
		

@stop