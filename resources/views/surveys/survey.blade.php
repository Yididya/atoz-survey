<meta name="csrf-token" content="{{ csrf_token() }}" />
@extends('app')

@section('title')
	A-Z Survey | Survey
@stop
@section('navigation_active_survey')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}
	.search{
		margin-top: 23px;
		margin-bottom: 10px
	}
	.content{
		margin-top: 20px;
	}
	a.category:before {
	    content: "#";
	    margin-left: -25px;
	    
	    font-size: 28px;
	    top: 5px;
	    color: #f4645f;
	    opacity: .6;
	}
	a.category{
		text-decoration: none;
	}
	
</style>
		<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    
			$("#previous").click(function(){
				$("#survey_form").submit();
			});
			$("#next").click(function(){
				$("#survey_form").submit();
			});
		});

		</script>


		<div class="a2z-jumbotron">
	        <div class="container">
	        		<div class="row">
	        			<div class="col-lg-6"><h1><a href="/home">A-Z </a>/ <a href="/projects">Surveys</a> </h1></div>
	        			<div class="col-lg-6">
	    
	        			</div>
	        			
	        		</div>
	                
	        </div>
		</div>
		
		<div class="container">
						
				
				
					
				
						<h1 class="text-center">Welcome to {{$project->title}} survey</h1>	
						<p class="text-center lead">{{$project->description}}</p>	
						<hr>
						<p>Please fill the form carefully and we kindly ask for your honesty in your answers </p>

						{!!Form::open(array('method'=>'post','id'=>'survey_form'))!!}

						@forelse($categories as $category)
							<h2 id="{{$category->category}}"><a class="category"href="#{{$category->category}}"> {{$category->category}}</a></h2>

							@forelse($category->questions as $question)
								<!-- If questions is has child questions -->
								
								@if(!empty($question->children_questions))

									<h3>{{$question->question}}</h3>
									<div class="container">
										<div class="col-lg-offset-1 col-lg-11">
											@foreach($question->children_questions as $child_question)
												<h4>{{$child_question->question}}</h4>
												<ul class="list-unstyled">
													@forelse($question->choices as $choice_number => $choice)
														
														@if(Input::old($child_question->id))
															@if(Input::old($child_question->id) == $choice_number)
																<li><label><input type="radio" required name="{{$child_question->id}}" value="{{$choice_number}}" checked>	{{$choice}}</label></li>
															
															@else
																<li><label><input type="radio" required name="{{$child_question->id}}" value="{{$choice_number}}">	{{$choice}}</label></li>
															@endif
														@else
															<li><label><input type="radio" required name="{{$child_question->id}}" value="{{$choice_number}}">	{{$choice}}</label>	</li>
														@endif


													@empty
													@endforelse
												</ul>
												<hr>
											@endforeach
											
										</div>
									</div>
								@else
									<h3>{{$question->question}}</h3>
									<div class="container">

										<ul class="list-unstyled">
														@forelse($question->choices as $choice_number => $choice)
															
														@if(Input::old($question->id))
															@if(Input::old($question->id) == $choice_number)
																<li><label><input type="radio" required name="{{$question->id}}" value="{{$choice_number}}" checked>	{{$choice}}</label></li>
															
															@else
																<li><label><input type="radio" required name="{{$question->id}}" value="{{$choice_number}}">	{{$choice}}</label></li>
															@endif
														@else
															<li><label><input type="radio" required name="{{$question->id}}" value="{{$choice_number}}">	{{$choice}}</label>	</li>
														@endif
														@empty
														@endforelse
										</ul>
									</div>

								@endif
							

							@empty

							@endforelse
							
								
						@empty
						@endforelse
						<div class="form-group text-center">
									<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
									<!-- <button id ="next" class="btn btn-success"><i class="fa fa-chevron-right"></i></button>			 -->
						</div>
												
						{!!Form::close()!!}
							
			
		</div>
	
@stop