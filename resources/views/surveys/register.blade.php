@extends('app')

@section('title')
	A-Z Survey | Projects
@stop
@section('navigation_active_survey')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}
	.search{
		margin-top: 23px;
		margin-bottom: 10px
	}

</style>
		<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    
			// $(".filter a").click(function(e){
			// 	e.preventDefault();

			// });
		});

		</script>

		<div class="a2z-jumbotron">
	        <div class="container">
	        		<div class="row">
	        			<div class="col-lg-6"><h1><a href="/home">A-Z </a>/ <a href="/projects">Surveys</a> </h1></div>
	        			<div class="col-lg-6">
	        				<form role="form">
	        					<input type="text" name ="q" placeholder="Search for surveys" class="form-control search" >
	        				</form>

	        			</div>
	        			
	        		</div>
	                
	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				<!-- Sidebar -->
				<div class="col-md-3">
					
				</div>

				<div class="col-md-9">
					
							<div class="row">
								<div class="col-lg-10">
									<h2>Register Respondent </h2>	
									<hr>
									<h4>BASIC INFORMATION</h4>
									@if(count($errors) >0)
		                            <div class="alert alert-danger">
		                                <strong>Oops!</strong> There were some problem with your input
		                                <ul>
		                                    @foreach($errors->all() as $error)
		                                        <li>{{ $error }}</li>
		                                    @endforeach
		                                </ul>   
		                            </div>
		                        	@endif
									{!! Form::open(array('url' => '/surveys/add/'.$survey_link, 'class' => 'form')) !!}
											<div class="form-group">
										    {!! Form::label('First name') !!}
										    {!! Form::text('first_name', null, 
										        array('required', 
										              'class'=>'form-control', 
										              'placeholder'=>'First name')) !!} 
										</div>

										<div class="form-group">
										    {!! Form::label('Last name') !!}
										    {!! Form::text('last_name', null, 
										        array('required', 
										              'class'=>'form-control', 
										              'placeholder'=>'Last name')) !!} 
										</div>

										<div class="form-group">
										    {!! Form::label('Email') !!}
										    {!! Form::email('email', null, 
										        array('class'=>'form-control', 
										              'placeholder'=>'Email')) !!} 
										</div>
										@forelse($segmentations as $segmentation)
												{!! Form::label($segmentation->name)!!}

												
													<select name="{{ $segmentation->column_name }}" class="form-control">

													@forelse($segmentation->values as $key => $value)
														<option value="{{$value}}">{{$value}}</option>
													@empty
													@endforelse
													</select>
												
										@empty

										@endforelse

										<div class="form-group">
										    {!! Form::submit('Continue', 
										      array('class'=>'btn btn-success')) !!}
										</div>


									{!! Form::close() !!}
								</div>
								
								

											
							</div>
							
							<div class="row ">
								
							</div>

							<div class="survey_list">
								
							</div>



							
							
							


					
				</div>
				

			</div>
		</div>
		

@stop