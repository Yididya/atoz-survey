<meta name="csrf-token" content="{{ csrf_token() }}" />
@extends('app')

@section('title')
	A-Z Survey | Survey
@stop
@section('navigation_active_survey')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}
	.search{
		margin-top: 23px;
		margin-bottom: 10px
	}
	.content{
		margin-top: 20px;
	}
	a.category:before {
	    content: "#";
	    margin-left: -25px;
	    
	    font-size: 28px;
	    top: 5px;
	    color: #f4645f;
	    opacity: .6;
	}
	a.category{
		text-decoration: none;
	}
	
</style>
		<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    
			$("#previous").click(function(){
				$("#survey_form").submit();
			});
			$("#next").click(function(){
				$("#survey_form").submit();
			});
		});

		</script>


		<div class="a2z-jumbotron">
	        <div class="container">
	        		<div class="row">
	        			<div class="col-lg-6"><h1><a href="/home">A-Z </a>/ <a href="/projects">Surveys</a>/ Survey successful </h1></div>
	        			<div class="col-lg-6">
	    
	        			</div>
	        			
	        		</div>
	                
	        </div>
		</div>
		
		<div class="container">
						
				
				
					
				
						<h1 class="text-center">You have successfully filled out the survey.</h1>	
						
						<hr>
						<p>We would like to thank you for both your time and honesty. </p>
						<a href="/surveys">Go back to surveys</a>
						
							
			
		</div>
	
@stop