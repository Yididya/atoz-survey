@extends('app')

@section('title')
	A-Z Survey | Projects
@stop
@section('navigation_active_survey')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}
	.search{
		margin-top: 23px;
		margin-bottom: 10px
	}

</style>
		<script type="text/javascript">
		$(document).ready(function(){
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    
			// $(".filter a").click(function(e){
			// 	e.preventDefault();

			// });
		});

		</script>

		<div class="a2z-jumbotron">
	        <div class="container">
	        		<div class="row">
	        			<div class="col-lg-6"><h1><a href="/home">A-Z </a>/ <a href="/projects">Surveys</a> </h1></div>
	        			<div class="col-lg-6">
	        				<form role="form" method="POST" action="">
                        	<input  type="hidden" name="_token" value ="{{ csrf_token()}}">
                        		@if(isset($query['q']))
		        					<input type="text" name ="q" placeholder="Search for surveys" class="form-control search" required value="{{ $query['q']}}" >
	        					@else
	        						<input type="text" name ="q" placeholder="Search for surveys" class="form-control search" required >
	        					@endif
	        				</form>

	        			</div>
	        			
	        		</div>
	                
	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					
				</div>

				<div class="col-md-9">
					
							<div class="row">
								<div class="col-lg-12"><h2>Surveys </h2>	

									@if(Session::has('message'))
								        <div class="alert alert-success">
								            <h2>{{ Session::get('message') }}</h2>
								        </div>
							    	@endif
								</div>
								
								
								
							</div>
							<hr>

							<div class="survey_list">
								@forelse($projects as $project)
									<ul class="list-unstyled">
									<li class="row project_list_item">
						                
						                    <div>
						                    	
							                        <div class="col-sm-12 col-lg-12">
							                                
							                                <h3>
							                                	
								                             		<a title="Link to the survey" href="/surveys/{{$project['survey']->link}}"><i class="fa fa-chain"></i> {{$project["title"]}}</a>
								                             	
							                                		
							                                	

							                             		
							                                	<a href="" class="pull-right" title="Edit project"><i class="fa fa-pencil"></i> </a>
							                                	<a href="" class="pull-right" title="Delete project"> <i class="fa fa-trash"></i> </a>
							                                </h3>
							                             	<p>{{$project["description"] }}
							                             	<span class="pull-right">Date created :{{$project["created_at"]}} </span></p>
							                             	
							                             	

							                             
							                        </div>
							                        <div class="col-sm-3 col-lg-2">
							                            
							                        </div>
						                        
						                    </div>
						                
						            </li>

									</ul>    
									@empty
								    	<h3>No surveys yet</h3>
									@endforelse
								</div>



							
							
							


					
				</div>
				

			</div>
		</div>
		

@stop