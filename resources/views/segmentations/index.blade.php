@extends('app')

@section('title')
	A-Z Survey | Respondents
@stop
@section('navigation_active_admin')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}

</style>
		<script type="text/javascript">
			$(document).ready(function(){
			
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
			$(".delete").click(function(){
				//Get the project Id 
				//console.log();
				$("#segmentation_id").text( $(this).parent().siblings(".segmentation_id").eq(0).text() );
				$('#myModal').modal('show');


			});
			$(".confirm_delete").click(function(){

				

				$.ajax({
					type:"GET",
					url:"/projects/{{$project->id}}/segmentations/delete/"+ $("#segmentation_id").text(),
					//data:{"_token":$('input[name="_token"]').attr('value'),"project_id" : $("#project_id").text()},
					success : function(result){
						console.log("Hello");
						$("#myModal").modal('hide');
						$("#list_item_"+$("#segmentation_id").text()).fadeOut(1000,function(){
							$(this).remove();	
						});

						
					},
					dataType:"json"
				});
				
			});
		});



		</script>
		<div class="a2z-jumbotron">
	        <div class="container">
	                <h1><a href="/projects/{{$project->id}}">{{$project->title}}</a> / Segmentations</h1>
	        </div>
		</div>
		<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Confirmation</h4>
		      </div>
		      <div class="modal-body">
		        <p>You are about to delete a filter</p>
		        <span style="display:none" id="segmentation_id"></span>
		        
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary confirm_delete">Confirm</button>
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					@include("sidebar")
				</div>

				<div class="col-md-9">

					
							<h2>Segmentations</h2>
							<hr>
							
							<table class="table">
								<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Type</th>
									<th>Manage Segmentations</th>
								</tr>
								</thead>
								<tbody>
									@forelse($segmentations as $segmentation)
										<tr id="list_item_{{$segmentation->id}}">
											<td class="segmentation_id">{{$segmentation->id}}</td>
											<td><a href="/projects/{{$project->id}}/segmentations/{{$segmentation->id}}">{{$segmentation->name}}</a></td>
											<td>{{$segmentation->type}}</td>
											<td><a href="/projects/{{$project->id}}/segmentations/{{$segmentation->id}}/edit"  title="Edit Segmentation"><i class="fa fa-pencil"></i> Edit</a>
						                                	<a href="javascript:void(0);"  class="delete" title="Delete segmentation"> <i class="fa fa-trash"></i> Delete</a></td>
										</tr>

									@empty
											Segmentations are filters that you can use for the data during analysis.<br>
											No segmentations yet. Click 
											<a href="/projects/{{$project->id}}/segmentations/add">here</a> to create some.
										
									@endforelse
									
								</tbody>


							</table>		
						
					
				</div>
				

			</div>
		</div>
		

@stop