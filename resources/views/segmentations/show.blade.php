@extends('app')

@section('title')
	A-Z Survey | Segmentation
@stop
@section('navigation_active_admin')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}

</style>
		<div class="a2z-jumbotron">
	        <div class="container">
	                <h1><a href="/projects/{{$project->id}}">{{$project->title}}</a> / Segmentation</h1>
	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					<div class="panel panel-default">
						@include("sidebar")
					</div>
				</div>

				<div class="col-md-9">
					
							<h2>Segmentation</h2>
							<hr>
							<h3>
								{{ $segmentation->name }}
							</h3>
							
							
							
							
							
							@if(!empty($segmentation_values))
								<h3>Segmentation Values</h3>
								<hr>
								<ul class="list-unstyled">
									@forelse($segmentation_values as $value)
									    <li>{{ $value->value }}</li>
									@empty
									    <p>No segmentation values</p>
									@endforelse
								</ul>

							@endif
							
					
				</div>
				

			</div>
		</div>
		

@stop