@extends('app')

@section('title')
	A-Z Survey | Respondents
@stop
@section('navigation_active_admin')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}

</style>



		<div class="a2z-jumbotron">
	        <div class="container">
	                <h1><a href="/projects/{{$project->id}}">{{$project->title}}</a> / Add Respondents</h1>
	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					<div class="panel panel-default">
						@include("sidebar")
					</div>
				</div>

				<div class="col-md-9">
					
							<h2>Add a new segmentation</h2>
							<hr>
							@if(!empty(session("message")))
								<div class="alert alert-info">
	                                <strong>Notice!</strong> {{ session('message') }}
	                            </div>
							@endif
							@if(count($errors) > 0)
	                            <div class="alert alert-danger">
	                                <strong>Oops!</strong> There were some problem with your input
	                                <ul>
	                                    @foreach($errors->all() as $error)
	                                        <li>{{ $error }}</li>
	                                    @endforeach
	                                </ul>   
	                            </div>
	                        @endif
							{!! Form::open(array('url' => '/projects/'.$project->id.'/segmentations/add', 'class' => 'form')) !!}
								<div class="form-group">
								    {!! Form::label('Segmentation Name') !!}
								    {!! Form::text('name', null, 
								        array('required', 
								              'class'=>'form-control', 
								              'placeholder'=>'Segentation Name')) 
								    !!} 
								</div>
								
								<!-- Segmentation values goes here -->
								
									<div class="row form-group">
										<div class="col-lg-1"><strong>+/-</strong></div>
										<div class="col-lg-11"><strong>Segmentation Value</strong></div>
									</div>
									
									<div class="form-group row" id="segmentation_0" style="display:none;">
										
											<div class="col-lg-1">
												<button title="click to add/remove the row" type="button" class="btn btn-xs btn-default"><i class='fa fa-minus-circle alert-danger'></i></button>	
											</div>
											<div class="col-lg-11">
												<input type="text" class="form-control" name="segmentation_value">
											</div>
											<br>
										
									</div>
									<div class="form-group" id="add_segmentation">
										<button title="click to add/remove the row" type="button" class="btn btn-xs btn-default"><i class="fa fa-plus-circle alert-success"></i></button>	
										
									</div>


									

									<br>
								
								


								<script type='text/javascript'>
									

										var lastRow=0;
										$("#add_segmentation button").click(function(){
											addChoice();

										});
										function removeChoice(x) {
											$("#segmentation_"+x).remove();
										}	
										function addChoice() {

											lastRow++;
											$("#segmentation_0").clone(true).attr('id','segmentation_'+lastRow).removeAttr('style').insertBefore("#add_segmentation");
											$("#segmentation_"+lastRow+" button").attr('onclick','removeChoice('+lastRow+')');
											$("#segmentation_"+lastRow+" input:first").attr('name','segmentation_value[]').attr('id','segmentation_value_'+lastRow);
										}
									
										
										
										

									
									
								</script>
								
								
								
								<div class="form-group">
								    {!! Form::submit('Add Segmentation!', 
								      array('class'=>'btn btn-success')) !!}
								</div>
								{!!Form::close()!!}
							

					
				</div>
				

			</div>

		</div>
			

@stop