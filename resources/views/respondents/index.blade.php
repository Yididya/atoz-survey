@extends('app')

@section('title')
	A-Z Survey | Respondents
@stop
@section('navigation_active_admin')
 active
@stop
@section('content')
<style type="text/css">
	.project_list_item{
		background: #fff;
	}

</style>
		<div class="a2z-jumbotron">
	        <div class="container">
	                <h1><a href="/projects/{{$project->id}}">{{$project->title}}</a> / Respondents</h1>
	        </div>
		</div>
		
		<div class="container">
			<div class="row">
				
				<div class="col-md-3">
					@include("sidebar")
				</div>

				<div class="col-md-9">
					
							<h2>Respondents</h2>
							<hr>
							
							<table class="table">
								<thead>
								<tr>
									<th>First Name</th>
									<th>Middle Name</th>
									<th>Last Name</th>
									<th>Manage User</th>
								</tr>
								</thead>
								<tbody>
									@foreach($respondents as $respondent)
										<tr>
											<td>{{$respondent->first_name}}</td>
											<td>{{$respondent->middle_name}}</td>
											<td>{{$respondent->last_name}}</td>
											<td><a href="/projects/{{$project->id}}/respondents/{{$respondent->id}}/edit"  title="Edit respondent"><i class="fa fa-pencil"></i> Edit</a>
						                                	<a href=""  title="Delete respondent"> <i class="fa fa-trash"></i> Delete</a></td>
										</tr>

									@endforeach
									
								</tbody>


							</table>		
						
					
				</div>
				

			</div>
		</div>
		

@stop